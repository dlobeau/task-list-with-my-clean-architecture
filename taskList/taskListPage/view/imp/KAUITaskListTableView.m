//
//  KAUITaskListTableView.m
//  taskList
//
//  Created by Didier Lobeau on 24/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAUITaskListTableView.h"
#import "KAUITasksListTableViewCell.h"

@implementation KAUITaskListTableView

- (void)tableView:(KAUITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    KAUITasksListTableViewCell *Cell = [tableView cellForRowAtIndexPath:indexPath];
     [Cell clickWithSender:tableView];
}

@end
