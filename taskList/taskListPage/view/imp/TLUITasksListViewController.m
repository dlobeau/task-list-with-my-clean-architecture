//
//  KAUITasksListViewController.m
//  taskList
//
//  Created by Didier Lobeau on 26/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUITasksListViewController.h"
#import "TLTaskListWindowWithHeader.h"
#import "KAUITableView.h"
#import "TLUIBarButtonFacade.h"

@interface TLUITasksListViewController ()

@property (weak) id<TLTaskListWindowWithHeader> interface;
  
@property (weak, nonatomic) IBOutlet KAUITableView *table;

@property TLUIBarButtonFacade * rightBarButton;

@end

@implementation TLUITasksListViewController


-(BOOL) addChildView:(id<KAView>)ChildView
{
    BOOL returnValue = YES;
    
    if( [ChildView.interface.ID isEqual:@"rightButton"]  )
    {
         self.rightBarButton = ( TLUIBarButtonFacade *)ChildView;
        [self.rightBarButton setAction:@selector(rightButtonAction)];
        [self.rightBarButton setTarget:self];
        UINavigationItem *BarItem = self.navigationItem;
        BarItem.rightBarButtonItem = self.rightBarButton;
    }
    ChildView.delegatePushNextView = self.delegatePushNextView;
    return returnValue;
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.interface initializeWithData];
    [self setContent];
}

-(IBAction)rightButtonAction
{
    [self.rightBarButton actionFromOwnerWindow:self];
}

-(BOOL) setContent
{
    [self.rightBarButton setContent];
    [self.table setContent];
    return NO;
}



@end
