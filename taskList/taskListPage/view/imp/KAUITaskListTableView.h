//
//  KAUITaskListTableView.h
//  taskList
//
//  Created by Didier Lobeau on 24/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

@import cleanProjectFramework;

NS_ASSUME_NONNULL_BEGIN

@interface KAUITaskListTableView : KAUITableView

@end

NS_ASSUME_NONNULL_END
