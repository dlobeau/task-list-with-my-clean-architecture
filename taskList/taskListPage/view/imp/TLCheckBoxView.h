//
//  TLCheckBoxView.h
//  taskList
//
//  Created by Didier Lobeau on 13/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUIViewFacade.h"

NS_ASSUME_NONNULL_BEGIN

@interface TLCheckBoxView : TLUIViewFacade

-(void) tickCheckBox;
-(BOOL) isCheckBocxTicked;

@end

NS_ASSUME_NONNULL_END
