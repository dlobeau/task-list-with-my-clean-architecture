//
//  KAUITasksListViewController.h
//  taskList
//
//  Created by Didier Lobeau on 26/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUIFacadeViewController.h"
#import "KAView.h"
NS_ASSUME_NONNULL_BEGIN

@interface TLUITasksListViewController : TLUIFacadeViewController 

@end

NS_ASSUME_NONNULL_END
