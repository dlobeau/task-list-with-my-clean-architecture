//
//  TLCheckBoxView.m
//  taskList
//
//  Created by Didier Lobeau on 13/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLCheckBoxView.h"
#import "KACheckBox.h"


@interface TLCheckBoxView()
@property (weak, nonatomic) IBOutlet UIView *checkMarkerView;

@end

@implementation TLCheckBoxView

-(BOOL) setContent
{
    id<KACheckBox> CheckBox = (id<KACheckBox>)[self interface];
    self.checkMarkerView.hidden = CheckBox.value;
    return YES;
}

-(void) tickCheckBox
{
    self.checkMarkerView.hidden = !self.checkMarkerView.hidden;
    
    id<KACheckBox> CheckBox = (id<KACheckBox>)[self interface];
    [CheckBox addCommandOnPresenterModificationWithBoolean:!self.checkMarkerView.hidden WithSender:CheckBox];
    [CheckBox executeCommandHierarchy];
}

-(BOOL) isCheckBocxTicked
{
    return !self.checkMarkerView.isHidden;
}



@end
