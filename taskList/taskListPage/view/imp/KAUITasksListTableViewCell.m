//
//  KAUITableViewCell.m
//  taskList
//
//  Created by Didier Lobeau on 29/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAUITasksListTableViewCell.h"
#import "TLTaskListTableCell.h"
#import "KAUILabel.h"
#import "TLCheckBoxView.h"
@interface KAUITasksListTableViewCell()

@property (weak) id<TLTaskListTableCell> interface;

@property (weak, nonatomic) IBOutlet KAUILabel *labelName;

@property (weak, nonatomic) IBOutlet KAUILabel *labelDeadLine;
@property (weak, nonatomic) IBOutlet TLCheckBoxView *checkBox;

@end

@implementation KAUITasksListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void) clickWithSender:(id<KAView>) Sender
{
    [self.checkBox tickCheckBox];
    [self.interface addCommandOnChildPresenterModification:self.checkBox.interface WithSender:self.interface ];
    [self.interface executeCommandHierarchy];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
}

#pragma KAView protocol #######################




-(BOOL) setContent
{
    [self.labelName setContent];
    [self.labelDeadLine setContent];
    [self.checkBox setContent];
    
    return YES;
}


@end
