//
//  KAUITableViewCell.h
//  taskList
//
//  Created by Didier Lobeau on 29/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUITableViewCellFacade.h"
#import "KAView.h"
NS_ASSUME_NONNULL_BEGIN

@interface KAUITasksListTableViewCell : TLUITableViewCellFacade<KAView>

-(void) clickWithSender:(id<KAView>) Sender;

@end

NS_ASSUME_NONNULL_END
