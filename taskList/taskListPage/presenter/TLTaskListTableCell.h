//
//  TLTaskListTableCell.h
//  taskList
//
//  Created by Didier Lobeau on 22/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAPresenter.h"
#import "KACell.h"
@protocol KALabel;
@protocol KACheckBox;
@protocol TLTaskDescriptionWindow;
NS_ASSUME_NONNULL_BEGIN

@protocol TLTaskListTableCell <KACell>

-(id<KALabel>) deadlineValueLabel;
-(id<KALabel>) nameValueLabel;
-(id<KACheckBox>) taskStatusCheckBox;

-(id<TLTaskDescriptionWindow>) editTaskWindow;
@end

NS_ASSUME_NONNULL_END
