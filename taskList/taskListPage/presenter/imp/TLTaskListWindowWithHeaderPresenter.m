//
//  TLTaskListWindowWithHeaderPresenter.m
//  taskList
//
//  Created by Didier Lobeau on 26/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLTaskListWindowWithHeaderPresenter.h"
#import "TLTasksDataBase.h"
#import "TLTaskDescriptionWindow.h"
#import "TLTaskListTable.h"
#import "TLAddBarButton.h"
#import "KAEvent.h"
#import "KACreateWindowFactory.h"

@interface TLTaskListWindowWithHeaderPresenter()

@property id<TLTaskListTable> table;
@property id<TLAddBarButton> addBarButton;

@end

@implementation TLTaskListWindowWithHeaderPresenter

@synthesize table = _table;
@synthesize addBarButton = _addBarButton;


-(id) initWithLabel:(NSString *)label WithLabelIdentifier:(NSString *)ID WithObjectFamilyName:(NSString *)GroupId WithID:(NSString *)TypeId
{
    self = [super initWithLabel:label WithLabelIdentifier:ID WithObjectFamilyName:GroupId WithID:TypeId];
    self.testdelegate = [[TLTaskListWindowWithHeaderPresenterConformToDomain alloc] init];
    self.viewFactoryDelegate = [[KACreateWindowFactory alloc] init];
    return self;
}

-(id<TLTaskListTable>) table
{
    if(_table == nil)
    {
        _table = (id<TLTaskListTable>)[self getChildwithIdentifier:@"taskListTable"];
    }
    NSAssert([_table conformsToProtocol:@protocol(TLTaskListTable)], @"table not defined in taskListWindowWithHeader, check %@", self.attributeIdentity);
    return _table;
}
-(void) setTable:(id<TLTaskListTable>)table
{
    _table = table;
}

-(id<TLAddBarButton>) addBarButton
{
    if(_addBarButton == nil)
    {
        _addBarButton =(id<TLAddBarButton>) [self getChildwithIdentifier:@"addBarButton"];
    }
    NSAssert([_addBarButton conformsToProtocol:@protocol(TLAddBarButton)], @"addBarButton should be defined in TLTaskListWindowWithHeader, check %@", self.attributeIdentity);
    return _addBarButton;
}
-(void) setAddBarButton:(id<TLAddBarButton>)addBarButton
{
    _addBarButton = addBarButton;
}

-(id<TLTaskDescriptionWindow>) taskCreationPage
{
    id<TLTaskDescriptionWindow> ReturnValue =nil;
   
    ReturnValue = (id<TLTaskDescriptionWindow>)[self.addBarButton nextAddWindow];
    
    NSAssert([ReturnValue conformsToProtocol:@protocol(TLTaskDescriptionWindow)], @"taskCreationPage should be accessible from TLTaskListWindowWithHeader, check %@",self.attributeIdentity);
    return ReturnValue;
}

@end

@implementation TLTaskListWindowWithHeaderPresenterConformToDomain

-(void) expectSender: (id<TLTaskListWindowWithHeader>) Sender conformToDomain:(id<TLTasksDataBase>) Domain
{
    NSAssert([Domain conformsToProtocol:@protocol(TLTasksDataBase)], @"TLTaskListWindowWithHeaderPresenter should be test with TLTasksDataBase data");
    
    [Sender.table.testdelegate expectSender:Sender.table conformToDomain:Domain ];
   
}

@end
