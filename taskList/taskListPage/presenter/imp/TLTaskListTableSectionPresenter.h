//
//  TLTaskListTableSectionPresenter.h
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAGenericPresenter.h"
#import "TLTaskListTableSection.h"
#import "KAPresenterConformToDomain.h"

NS_ASSUME_NONNULL_BEGIN

@interface TLTaskListTableSectionPresenter : KAGenericPresenter<TLTaskListTableSection>

@end

@interface TLTaskListTableSectionPresenterConformToDomain:NSObject< KAPresenterConformToDomain>


@end

NS_ASSUME_NONNULL_END
