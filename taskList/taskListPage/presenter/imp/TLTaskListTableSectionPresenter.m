//
//  TLTaskListTableSectionPresenter.m
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLTaskListTableSectionPresenter.h"
#import "TLTaskList.h"
#import "TLTask.h"
#import "TLTaskListTableCell.h"
#import "KACell.h"

@interface TLTaskListTableSectionPresenter()


@end

@implementation TLTaskListTableSectionPresenter



-(id) initWithLabel:(NSString *)label WithLabelIdentifier:(NSString *)ID WithObjectFamilyName:(NSString *)GroupId WithID:(NSString *)TypeId
{
    self = [super initWithLabel:label WithLabelIdentifier:ID WithObjectFamilyName:GroupId WithID:TypeId];
    self.testdelegate = [[TLTaskListTableSectionPresenterConformToDomain alloc] init];
    return self;
}

-(BOOL) isInitializedOnParentCreation
{
    return NO;
}

-(NSArray<id<TLTaskListTableCell>> *) cells
{
    NSMutableArray<id<TLTaskListTableCell>> *ReturnValue = nil;
    
    NSArray<id<TLTaskListTableCell>> *allCHilds = (NSArray<id<TLTaskListTableCell>> *)[self childInterface];
    
    for(int i = 0; i< allCHilds.count; i++)
    {
        id<TLTaskListTableCell> CurrentCell = [allCHilds objectAtIndex:i];
        if([CurrentCell conformsToProtocol: @protocol(TLTaskListTableCell) ])
        {
            if(ReturnValue == nil)
            {
                ReturnValue = [[NSMutableArray alloc] init];
            }
            [ReturnValue addObject:CurrentCell];
        }
        
    }
    return ReturnValue;
    
}

-(id<KACell>) cellAtIndex:(NSInteger) Index
{
    NSArray<id<TLTaskListTableCell>> *Cells = self.cells;
    NSAssert(Index< Cells.count,@"index: %ld can't be bigger than cell list: %ld",Index,Cells.count);
    
    return [Cells objectAtIndex:Index];
}


@end

@implementation TLTaskListTableSectionPresenterConformToDomain

-(void) expectSender: (id<TLTaskListTableSection>) Sender conformToDomain:(id<TLTaskList>) Domain
{
    NSAssert([Domain conformsToProtocol:@protocol(TLTaskList)], @"TLTaskListTableSection should be test with TLTasksDataBase data");
    
    NSArray<id<TLTask>> * taskList =  [Domain tasks];
    NSArray<id<TLTaskListTableCell>> * Cells = [Sender cells];
    
    NSAssert([taskList count] == [Cells count], @"cells number should be equals to tasks number: %ld instead of %ld, check %@",[taskList count],[Cells count], Sender.attributeIdentity);
    
    for(int i = 0; i< Cells.count ; i++)
    {
        id<TLTask> CurrentTask = [taskList objectAtIndex:i];
        id<TLTaskListTableCell> CurrentCell = [Cells objectAtIndex:i];
        
        [CurrentCell.testdelegate expectSender:CurrentCell conformToDomain:CurrentTask ];
    }
    
    
    
}

@end
