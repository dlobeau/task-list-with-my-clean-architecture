//
//  TLTaskListTablePresenter.m
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLTaskListTablePresenter.h"
#import "TLTaskListTableSection.h"
#import "TLTasksDataBase.h"
#import "KASection.h"
#import "KACreateUIWidgetTable.h"

@interface TLTaskListTablePresenter()

@property id<TLTaskListTableSection> allTaskSection;

@end


@implementation TLTaskListTablePresenter

@synthesize allTaskSection = _allTaskSection;

-(id) initWithLabel:(NSString *)label WithLabelIdentifier:(NSString *)ID WithObjectFamilyName:(NSString *)GroupId WithID:(NSString *)TypeId
{
    self = [super initWithLabel:label WithLabelIdentifier:ID WithObjectFamilyName:GroupId WithID:TypeId];
    self.testdelegate = [[TLTaskListTablePresenterConformToDomain alloc] init];
    self.viewFactoryDelegate = [[KACreateUIWidgetTable alloc] init];
    
    return self;
}

-(NSArray<id<KASection>> *) sections
{
    NSMutableArray<id<KASection>> *ReturnValue = nil;
    
    NSArray<id<KASection>> *allCHilds = (NSArray<id<KASection>> *)[self childInterface];
    
    for(int i = 0; i< allCHilds.count; i++)
    {
        id<KASection> CurrentSection = [allCHilds objectAtIndex:i];
        if([CurrentSection conformsToProtocol: @protocol(KASection) ])
        {
            if(ReturnValue == nil)
            {
                ReturnValue = [[NSMutableArray alloc] init];
            }
            [ReturnValue addObject:CurrentSection];
        }
        
    }
    return ReturnValue;
}

-(id<KASection>) sectionAtIndex:(NSInteger) Index
{
    NSArray<id<KASection>> *Sections = self.sections;
    NSAssert(Index< Sections.count,@"index: %ld can't be bigger than Section list: %ld",Index,Sections.count);
    
    return [Sections objectAtIndex:Index];
}

-(id<TLTaskListTableSection>) allTaskSection
{
    if(_allTaskSection == nil)
    {
        _allTaskSection = (id<TLTaskListTableSection>)[self sectionAtIndex:0];
    }
    NSAssert([_allTaskSection conformsToProtocol:@protocol(TLTaskListTableSection)], @"section not defined in taskListtable, check %@", self.attributeIdentity);
    return _allTaskSection;
}
-(void) setAllTaskSection:(id<TLTaskListTableSection>)allTaskSection
{
    _allTaskSection = allTaskSection;
}

-(NSSet<NSString *> *) getHeaderTypeList
{
    NSMutableSet<NSString *> *ReturnValue = nil;
    
    NSArray<id<KASection> > *SectionList = [self sections];
    
    for(int i = 0; i < [SectionList count]; i++)
    {
        if(ReturnValue == nil)
        {
            ReturnValue = [[NSMutableSet alloc]init];
        }
        id<KASection> CurrentSection = [SectionList objectAtIndex:i];
        if(CurrentSection.header != nil)
        {
            [ReturnValue addObject:[CurrentSection.header labelIdentifier]];
        }
    }
    return ReturnValue;
}

@end

@implementation TLTaskListTablePresenterConformToDomain

-(void) expectSender: (id<TLTaskListTable>) Sender conformToDomain:(id<TLTasksDataBase>) Domain
{
    NSAssert([Domain conformsToProtocol:@protocol(TLTasksDataBase)], @"TLTaskListWindowWithHeaderPresenter should be test with TLTasksDataBase data");
    
    id<KADomain> SectionsDomainReference = [Domain requestWithID:@"allTasks"];
    
    NSAssert([Domain conformsToProtocol:@protocol(KADomain)], @"domain extraction for TLTaskListTable presenter check failled, check %@", Domain.attributeIdentity);
    
    [Sender.allTaskSection.testdelegate expectSender:Sender.allTaskSection conformToDomain:SectionsDomainReference ];
    
}

@end
