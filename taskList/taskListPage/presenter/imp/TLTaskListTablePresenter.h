//
//  TLTaskListTablePresenter.h
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAGenericPresenter.h"
#import "TLTaskListTable.h"
#import "KAPresenterConformToDomain.h"
NS_ASSUME_NONNULL_BEGIN

@interface TLTaskListTablePresenter : KAGenericPresenter<TLTaskListTable>



@end

@interface TLTaskListTablePresenterConformToDomain:NSObject< KAPresenterConformToDomain>


@end

NS_ASSUME_NONNULL_END
