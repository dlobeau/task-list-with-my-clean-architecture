//
//  TLTaskListWindowWithHeaderPresenter.h
//  taskList
//
//  Created by Didier Lobeau on 26/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAGenericPresenter.h"
#import "KAPresenterConformToDomain.h"
#import "TLTaskListWindowWithHeader.h"
NS_ASSUME_NONNULL_BEGIN

@interface TLTaskListWindowWithHeaderPresenter : KAGenericPresenter<TLTaskListWindowWithHeader>

@end

@interface TLTaskListWindowWithHeaderPresenterConformToDomain:NSObject< KAPresenterConformToDomain>


@end
NS_ASSUME_NONNULL_END
