//
//  TLTaskListCellCheckBox.h
//  taskList
//
//  Created by Didier Lobeau on 13/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KACheckBoxPresenter.h"

NS_ASSUME_NONNULL_BEGIN

@interface TLTaskListCellCheckBoxPresenter : KACheckBoxPresenter

@end

NS_ASSUME_NONNULL_END
