//
//  TLTaskListTableCellPresenter.m
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLTaskListTableCellPresenter.h"
#import "TLTask.h"
#import "KALabel.h"
#import "KADate.h"
#import "KACheckBox.h"
#import "TLTaskDescriptionWindow.h"
#import "KACellSwipeAction.h"
#import "KAEvent.h"

@interface TLTaskListTableCellPresenter()

@property id<KALabel> deadlineValueLabel;
@property id<KALabel> nameValueLabel;
@property id<KACheckBox> taskStatusCheckBox;
@property NSMutableArray<id<KACellSwipeAction>>* swipLeftEvents;

@end



@implementation TLTaskListTableCellPresenter

@synthesize deadlineValueLabel = _deadlineValueLabel;
@synthesize nameValueLabel = _nameValueLabel;
@synthesize taskStatusCheckBox = _taskStatusCheckBox;
@synthesize swipLeftEvents = _swipLeftEvents;

-(id) initWithLabel:(NSString *)label WithLabelIdentifier:(NSString *)ID WithObjectFamilyName:(NSString *)GroupId WithID:(NSString *)TypeId
{
    self = [super initWithLabel:label WithLabelIdentifier:ID WithObjectFamilyName:GroupId WithID:TypeId];
    self.testdelegate = [[TLTaskListTableCellPresenterConformToDomain alloc] init];
    return self;
}

-(id<KALabel>) deadlineValueLabel
{
    if(_deadlineValueLabel == nil)
    {
        _deadlineValueLabel = (id<KALabel>)[self getChildwithIdentifier:@"value1"];
    }
    NSAssert([_deadlineValueLabel conformsToProtocol:@protocol(KALabel)], @"_deadlineValueLabel not defined in task cell, check %@",self.attributeIdentity);
    return _deadlineValueLabel;
}
-(void)setDeadlineValueLabel:(id<KALabel>)deadlineValueLabel
{
    _deadlineValueLabel = deadlineValueLabel;
}
-(id<KALabel>) nameValueLabel
{
    if(_nameValueLabel == nil)
    {
        _nameValueLabel = (id<KALabel>)[self getChildwithIdentifier:@"value0"];
    }
    NSAssert([_nameValueLabel conformsToProtocol:@protocol(KALabel)], @"_nameValueLabel not defined in task cell, check %@",self.attributeIdentity);
    return _nameValueLabel;
}
-(void )setNameValueLabel:(id<KALabel>)nameValueLabel
{
    _nameValueLabel = nameValueLabel;
}

-(id<KACheckBox>) taskStatusCheckBox
{
    if(_taskStatusCheckBox == nil)
    {
        _taskStatusCheckBox = (id<KACheckBox>)[self getChildwithIdentifier:@"checkBox"];
    }
    NSAssert([_taskStatusCheckBox conformsToProtocol:@protocol(KACheckBox)], @"_taskStatusCheckBox not defined in task cell, check %@",self.attributeIdentity);
    return _taskStatusCheckBox;
}
-(void) setTaskStatusCheckBox:(id<KACheckBox>)taskStatusCheckBox
{
    _taskStatusCheckBox = taskStatusCheckBox;
}

-(id<TLTaskDescriptionWindow>) editTaskWindow
{
    id<TLTaskDescriptionWindow> ReturnValue = nil;
    
    NSArray<id<KACellSwipeAction>>* ListSwipAction = [self swipLeftEvents];
    if(ListSwipAction.count > 0)
    {
        id<KACellSwipeAction> EditSwapAction = [ListSwipAction objectAtIndex:0];
        id<KAEvent> Event = EditSwapAction.event;
        ReturnValue = ( id<TLTaskDescriptionWindow>)[Event getDestinationWithOwner:self];
        
    }
    NSAssert([ReturnValue conformsToProtocol:@protocol(TLTaskDescriptionWindow)],@"swip actiton must be defined on cell for Task edition, check %@",self.attributeIdentity);
    return ReturnValue;
}


-(NSArray<id<KACellSwipeAction>>*) swipLeftEvents
{
    if(_swipLeftEvents == nil)
    {
        NSArray<id<KAPresenter>> * List = [self childInterface];
        for(int i = 0; i< [List count]; i++)
        {
            id<KACellSwipeAction> CurrentAction = (id<KACellSwipeAction>)[List objectAtIndex:i];
            if([CurrentAction conformsToProtocol:@protocol(KACellSwipeAction)])
            {
                if(_swipLeftEvents == nil)
                {
                    _swipLeftEvents = [[NSMutableArray alloc] init];
                }
                [_swipLeftEvents addObject:CurrentAction];
            }
        }
    }
    return _swipLeftEvents;
}

-(void) setSwipLeftEvents:(NSMutableArray<id<KACellSwipeAction>> *)swipLeftEvents
{
    _swipLeftEvents = swipLeftEvents;
}

@end

@implementation TLTaskListTableCellPresenterConformToDomain

-(void) expectSender:(id<TLTaskListTableCell>)Sender conformToDomain:(id<TLTask>)Domain
{
     NSAssert([Domain conformsToProtocol:@protocol(TLTask)], @"TLTaskListTableCell should be test with TLTask data");
    [Sender.deadlineValueLabel.testdelegate expectSender:Sender.deadlineValueLabel conformToText:Domain.deadline.completeString];
    [Sender.nameValueLabel.testdelegate expectSender:Sender.nameValueLabel conformToText:Domain.name];
    [Sender.taskStatusCheckBox.testdelegate expectSender:Sender.taskStatusCheckBox conformToDomain:Domain.status];
}

@end
