//
//  TLTaskListTableSection.h
//  taskList
//
//  Created by Didier Lobeau on 29/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KASection.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TLTaskListTableCell;

@protocol TLTaskListTableSection <KASection>

-(NSArray<id<TLTaskListTableCell>> *) cells;

@end

NS_ASSUME_NONNULL_END
