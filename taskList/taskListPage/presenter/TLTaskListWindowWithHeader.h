//
//  TLTaskListWindowWithHeader.h
//  taskList
//
//  Created by Didier Lobeau on 22/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KAPresenter.h"
NS_ASSUME_NONNULL_BEGIN

@protocol TLTaskListTable;
@protocol TLTaskDescriptionWindow;

@protocol TLTaskListWindowWithHeader <KAPresenter>

-(id<TLTaskListTable>) table;
-(id<TLTaskDescriptionWindow>) taskCreationPage;

@end

NS_ASSUME_NONNULL_END
