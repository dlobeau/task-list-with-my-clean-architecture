//
//  TLTaskListTable.h
//  taskList
//
//  Created by Didier Lobeau on 22/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KATable.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TLTaskListTableSection;

@protocol TLTaskListTable <KATable>

-(id<TLTaskListTableSection>) allTaskSection;


@end

NS_ASSUME_NONNULL_END
