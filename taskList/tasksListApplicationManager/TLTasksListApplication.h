//
//  TLTasksListApplication.h
//  taskList
//
//  Created by Didier Lobeau on 12/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAApplication.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TLTasksListApplication <KAApplication>

-(id<TLTasksDataBase>) taskDataBase;

@end

NS_ASSUME_NONNULL_END
