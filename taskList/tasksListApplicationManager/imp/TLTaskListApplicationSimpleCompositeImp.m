//
//  TLTaskListApplicationSimpleCompositeImp.m
//  taskList
//
//  Created by Didier Lobeau on 12/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLTaskListApplicationSimpleCompositeImp.h"
#import "TLDelegateGenericTaskFactory.h"
#import "TLTasksDataBase.h"
#import "KASerializeObjectFactory.h"

@interface TLTaskListApplicationSimpleCompositeImp()

@property id<TLTasksDataBase> taskDataBase;

@end

@implementation TLTaskListApplicationSimpleCompositeImp

@synthesize taskDataBase = _taskDataBase;

-(void) initializaDataBase
{
    self.taskDataBase.taskFactory =  [[TLDelegateGenericTaskFactory alloc] init];
}

-(void) start
{
    [super start];
    NSDictionary *environment = [[NSProcessInfo processInfo] environment];
    id<KADomain> DataBinding = nil;
    
    NSString * DataBindingString = [environment objectForKey:@"dataBinding"];
    if(DataBindingString != nil)
    {
        DataBinding = (id<KADomain>)[self.factory createAttributeFromString:DataBindingString];
    }
    
    if(DataBinding != nil)
    {
        self.window.data = DataBinding;
    }
}

-(id<TLTasksDataBase>) taskDataBase
{
    if(_taskDataBase == nil)
    {
        _taskDataBase = (id<TLTasksDataBase>)[self.datBaseContener requestWithID:@"taskDataBase"];;
    }
    return _taskDataBase;
}
-(void)setTaskDataBase:(id<TLTasksDataBase>)taskDataBase
{
    _taskDataBase = taskDataBase;
}




@end
