//
//  TLTaskListApplicationSimpleCompositeImp.h
//  taskList
//
//  Created by Didier Lobeau on 12/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAApplicationSerializableObjectImp.h"
#import "TLTasksListApplication.h"
NS_ASSUME_NONNULL_BEGIN

@interface TLTaskListApplicationSimpleCompositeImp : KAApplicationSerializableObjectImp<TLTasksListApplication>

@end

NS_ASSUME_NONNULL_END
