//
//  TLTaskGeneric.h
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAGenericDomain.h"
#import "TLTask.h"

NS_ASSUME_NONNULL_BEGIN

@interface TLTaskGeneric : KAGenericDomain<TLTask>

-(id) initWithName:(NSString *) Name WithDeadline:(id<KADate>) Deadline;

-(id<TLTask>) NULL_TASK;

@end

NS_ASSUME_NONNULL_END
