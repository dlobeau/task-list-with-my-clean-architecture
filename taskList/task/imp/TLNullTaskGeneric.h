//
//  TLNullTaskGeneric.h
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLTaskGeneric.h"

NS_ASSUME_NONNULL_BEGIN

@interface TLNullTaskGeneric : TLTaskGeneric

@end

NS_ASSUME_NONNULL_END
