//
//  TLTaskGeneric.m
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLTaskGeneric.h"
#import "TLNullTaskGeneric.h"
#import "KADate.h"
#import "KABoolean.h"

#import "TLTasksDataBase.h"
#import "KASerializeObjectFactory.h"
@interface TLTaskGeneric()

@property id<KADate> deadline;
@property id<KABoolean> status;

@property (weak) id<TLTasksDataBase> owner;
@end

//task injected
@implementation TLTaskGeneric

@synthesize status = _status;


static NSString * NAME_ID = @"name";
static NSString * DEADLINE_ID = @"deadline";
static NSString * STATUS_ID = @"status";


-(id) initWithName:(NSString *) Name WithDeadline:(id<KADate>) Deadline
{
    self = [super initWithLabel:Name WithLabelIdentifier:Name WithObjectFamilyName:@"task" WithID:@"task"];
    self.name = Name;
    self.deadline = Deadline;
    return self;
}

-(NSString *) name
{
    return self.label;
}
-(void) setName:(NSString *) Name
{
    [self setLabel:Name];
}
-(id<KABoolean>) status
{
    if(_status == nil)
    {
        _status = (id<KABoolean>)[self getChildwithTypeId:STATUS_ID];
        if(_status == nil)
        {
            _status = (id<KABoolean>)[self.factoryDelegate createObjectFromFamilyType:@"bool"];
            [_status setValue:NO];
            [self setStatus:_status];
        }
    }
    return _status;
}
-(void) setStatus:(id<KABoolean>)status
{
    _status = (id<KABoolean>)[status cloneObject];
    _status.ID = STATUS_ID;
    [self addChild:(id<KASeriazableObject>)_status];
}





-(NSComparisonResult) compareAttribute:(id<TLTask>)AttributeToBeCompared
{
    NSAssert([AttributeToBeCompared conformsToProtocol:@protocol(TLTask)], @"task must be compared with task");
    NSComparisonResult Result = NSOrderedAscending;
    
    if( [self.name isEqual:AttributeToBeCompared.name]
       && ([self.deadline compareAttribute:AttributeToBeCompared.deadline] == NSOrderedSame) )
    {
        Result = NSOrderedSame;
    }
    return NSOrderedSame;
}

- ( NSString *)taskId {
   return self.ID;
}


-(id<TLTask>) NULL_TASK
{
    return [[TLNullTaskGeneric alloc] init];
}

-(BOOL)validate
{
    NSAssert(self.owner != nil, @"Task: %@ can't be validate without owner defined", self.name);
    
    BOOL ReturnValue = YES;
    
    id<TLTask> ExtractedFromDb = [self.owner taskWithID:self.ID];
    if(self.owner.NULL_TASK == ExtractedFromDb)
    {
        [self.owner addTask:self ];
    }
    
    return ReturnValue;
}

-(void) deleteItem
{
    [self.owner removeTaskWithId:self.ID];
}


-(id<KADomain>) requestWithID:(NSString *)RequestID
{
    id<KADomain> ReturnValue = [super requestWithID:RequestID];
    
    if([RequestID isEqual:DEADLINE_ID])
    {
        ReturnValue = (id<KADomain>)[self deadline ];
    }
    else if ([RequestID isEqual:NAME_ID])
    {
        ReturnValue = [KAGenericDomain createGenericObjectWithLabel:self.name];
    }
    else if ([RequestID isEqual:STATUS_ID])
    {
        ReturnValue = [self status];
    }
    
    return ReturnValue;
}

-(void) modifyChildDomainWithId:(NSString *)Id WithValue:(id<KADomain>)NewValue
{
    if([Id isEqual:NAME_ID])
    {
        [self setName:NewValue.label];
    }
    else if([Id isEqual:DEADLINE_ID])
    {
        NSAssert([NewValue conformsToProtocol:@protocol(KADate)], @"dealine modfication must be made with Date type");
        [self setDeadline:(id<KADate>)NewValue];
    }
    else if([Id isEqual:STATUS_ID])
    {
        NSAssert([NewValue conformsToProtocol:@protocol(KABoolean)], @"status modfication must be made with boolean type");
        [self setStatus:(id<KABoolean>)NewValue];
    }
}

@end
