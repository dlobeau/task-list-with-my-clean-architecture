//
//  TLTasksList.m
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLTasksListGeneric.h"
#import "TLTask.h"

@interface TLTasksListGeneric()

@property NSMutableArray<id<TLTask>> *taskLists;

@end

@implementation TLTasksListGeneric

@synthesize taskLists = _taskLists;

-(NSArray<id<TLTask>> *) tasks
{
   return self.taskLists;
}

-(void) addTask:(id<TLTask>) Task
{
    if(self.taskLists == nil)
    {
        self.taskLists = [[NSMutableArray alloc] init];
    }
    [self.taskLists addObject:Task];
}

-(NSArray<id<TLTask>> *) childs
{
    return self.taskLists;
}

+(id) createList
{
    return [[TLTasksListGeneric alloc] initWithLabel:@"taskList" WithLabelIdentifier:@"taskList" WithObjectFamilyName:@"taskList" WithID:@"taskList"];
}

-(id<KADomain>) requestWithID:(NSString *)RequestID
{
    id<KADomain> ReturnValue = [super requestWithID:RequestID];
    
    if([RequestID isEqual:@"allTasks"])
    {
        ReturnValue = self;
    }
    
    return ReturnValue;
}



@end
