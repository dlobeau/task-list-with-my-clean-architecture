//
//  TLNullTaskGeneric.m
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLNullTaskGeneric.h"


//NUL_TASK injected
@implementation TLNullTaskGeneric


-(id) init
{
    self = [super initWithLabel:@"NULL_TASK" WithLabelIdentifier:@"NULL_TASK" WithObjectFamilyName:@"NULL_TASK" WithID:@"NULL_TASK"];
    
    return self;
}


-(void) setName:(NSString *) Name
{
    
}

-(void) setDeadline:(id<KADate>) Deadline
{
    
}



-(BOOL) isTaskComplete
{
    return NO;
}

-(void) setTaskComplete
{
    
}
-(void) setTaskUncomplete
{
    
}

@end
