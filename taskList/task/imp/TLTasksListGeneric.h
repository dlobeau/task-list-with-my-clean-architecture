//
//  TLTasksList.h
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAGenericDomain.h"
#import "TLTaskList.h"
NS_ASSUME_NONNULL_BEGIN

//taskList Injected
@interface TLTasksListGeneric : KAGenericDomain<TLTaskList>

+(id) createList;

@end

NS_ASSUME_NONNULL_END
