//
//  TLTaskList.h
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KADomain.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TLTask;

@protocol TLTaskList <KADomain>

-(NSArray<id<TLTask>> *) tasks;
-(void) addTask:(id<TLTask>) Task;
@end

NS_ASSUME_NONNULL_END
