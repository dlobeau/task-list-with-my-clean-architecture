//
//  TLTask.h
//  taskList
//
//  Created by Didier Lobeau on 22/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KADomain.h"
NS_ASSUME_NONNULL_BEGIN

@protocol KADate;
@protocol TLTasksDataBase;
@protocol KABoolean;
@protocol TLTask <KADomain>

-(NSString *) name;
-(void) setName:(NSString *) Name;

-(NSString *) taskId;

-(id<KADate>) deadline;
-(void) setDeadline:(id<KADate>) Deadline;

-(id<KABoolean>) status;
-(void) setStatus:(id<KABoolean>) status;

-(id<TLTasksDataBase>) owner;
-(void) setOwner:(id<TLTasksDataBase>) owner;

@end



NS_ASSUME_NONNULL_END
