//
//  KAQueryOperatorAllItemFromDB.m
//  taskList
//
//  Created by Didier Lobeau on 22/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAQueryBusinessUseCaseAllItemFromDB.h"
#import "KADomain.h"
#import "TLTasksDataBase.h"
#import "KADomainLink.h"

//operatorAllItemFromDB
@implementation KAQueryBusinessUseCaseAllItemFromDB

-(id<KADomain>) executeWithRepository:(id<KADomain>) Repository WithValue:(id<KADomain>) Value
{
    id<TLTasksDataBase> DataBase = ( id<TLTasksDataBase>)Repository;
    if([DataBase conformsToProtocol:@protocol(KADomainLink)])
    {
        DataBase = (id<TLTasksDataBase>)[(id<KADomainLink>)DataBase source];
    }
    NSAssert([DataBase conformsToProtocol:@protocol(TLTasksDataBase)], @"KAQueryOperatorNewItemCreationFromDB should be call with TLTasksDataBase type repository, check %@", self.attributeIdentity);
    
    return [DataBase requestWithID:@"allTasks"];
}


@end
