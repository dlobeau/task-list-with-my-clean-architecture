//
//  KAQueryOperatorNewItemCreationFromDB.m
//  taskList
//
//  Created by Didier Lobeau on 22/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAQueryBusinessUseCaseNewItemCreationFromDB.h"
#import "TLTasksDataBase.h"
#import "TLTask.h"
#import "KADomainLink.h"

//operatorCreateNewItem
@implementation KAQueryBusinessUseCaseNewItemCreationFromDB

-(id<KADomain>) executeWithRepository:(id<TLTasksDataBase>) Repository WithValue:(id<KADomain>) Value
{
    id<TLTasksDataBase> DataBase = Repository;
    if([DataBase conformsToProtocol:@protocol(KADomainLink)])
    {
        DataBase = (id<TLTasksDataBase>)[(id<KADomainLink>)DataBase source];
    }
    NSAssert([DataBase conformsToProtocol:@protocol(TLTasksDataBase)], @"KAQueryOperatorNewItemCreationFromDB should be call with TLTasksDataBase type repository, check %@", self.attributeIdentity);
    
    return [DataBase createNewTask];
}



@end
