//
//  KAQueryOperatorNewItemCreationFromDB.h
//  taskList
//
//  Created by Didier Lobeau on 22/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KAQueryBusinessUseCase.h"
#import "KACompleteItemIdentification.h"
#import "KAGenericPresenter.h"
NS_ASSUME_NONNULL_BEGIN

@interface KAQueryBusinessUseCaseNewItemCreationFromDB : KAGenericPresenter<KAQueryBusinessUseCase>

@end

NS_ASSUME_NONNULL_END
