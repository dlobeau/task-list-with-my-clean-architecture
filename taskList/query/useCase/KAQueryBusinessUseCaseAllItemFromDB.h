//
//  KAQueryOperatorAllItemFromDB.h
//  taskList
//
//  Created by Didier Lobeau on 22/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//


#import "KAQueryBusinessUseCase.h"
#import "KASeriazableObjectTableImp.h"
NS_ASSUME_NONNULL_BEGIN

@interface KAQueryBusinessUseCaseAllItemFromDB : KASeriazableObjectTableImp<KAQueryBusinessUseCase>

@end

NS_ASSUME_NONNULL_END
