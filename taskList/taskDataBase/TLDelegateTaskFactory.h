//
//  TLDelegateTaskFactory.h
//  taskList
//
//  Created by Didier Lobeau on 22/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TLTask;
@protocol KADate;

@protocol TLDelegateTaskFactory <NSObject>

-(id<TLTask>) createTaskWithName:(NSString *) name WithDeadline:(id<KADate>) Date;
-(id<TLTask>) createNewTask;


@end

NS_ASSUME_NONNULL_END
