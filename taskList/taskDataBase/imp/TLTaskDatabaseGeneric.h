//
//  TLTaskDatabaseGeneric.h
//  taskList
//
//  Created by Didier Lobeau on 26/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAGenericDomain.h"
#import "TLTasksDataBase.h"
NS_ASSUME_NONNULL_BEGIN

@interface TLTaskDatabaseGeneric : KAGenericDomain<TLTasksDataBase>

@end

NS_ASSUME_NONNULL_END
