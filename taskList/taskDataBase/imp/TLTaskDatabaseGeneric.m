//
//  TLTaskDatabaseGeneric.m
//  taskList
//
//  Created by Didier Lobeau on 26/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLTaskDatabaseGeneric.h"
#import "TLTask.h"
#import "TLNullTaskGeneric.h"
#import "TLTasksListGeneric.h"
#import "TLDelegateTaskFactory.h"

@interface TLTaskDatabaseGeneric()

    @property NSMutableDictionary<NSString *,id<TLTask>> *taskList;
    @property id<TLTask> NULL_TASK;

    @property id<TLDelegateTaskFactory> taskFactory;
@end

//taskDataBase injected
@implementation TLTaskDatabaseGeneric

@synthesize NULL_TASK = _NULL_TASK;

-(NSArray<id<TLTask>> *) tasks
{
    return self.taskList.allValues;
}

-(void) addTask:(id<TLTask>) Task
{
   if(self.taskList == nil)
   {
       self.taskList = [[NSMutableDictionary alloc] init];
   }
    NSString *ID = self.createIDForNextInsertedTask;
    Task.ID = ID;
    [self.taskList setObject:Task forKey:ID];
}

-(NSString * ) createIDForNextInsertedTask
{
    return [NSString stringWithFormat:@"task_%ld", self.tasksCount];
}

-(NSInteger) tasksCount
{
    NSInteger ReturnVAlue = 0;
    if(self.taskList != nil)
    {
        ReturnVAlue = self.taskList.count;
    }
    return ReturnVAlue;
}

-(id<TLTask>) NULL_TASK
{
    if(_NULL_TASK ==nil)
    {
        _NULL_TASK = [[TLNullTaskGeneric alloc] init];
    }
    return _NULL_TASK;
}
-(void) setNULL_TASK:(id<TLTask>)NULL_TASK
{
    NULL_TASK = NULL_TASK;
}

-(void) removeTaskWithId:(NSString *) TaxId
{
    [self.taskList removeObjectForKey:TaxId];
}

-(id<KADomain>) requestWithID:(NSString *)RequestID
{
    id<KADomain> ReturnValue = [super requestWithID:RequestID];
    
    if([RequestID isEqual:@"allTasks"])
    {
        ReturnValue = [self allTaskObject];
    }
    
    return ReturnValue;
}

-(id<TLTaskList>) allTaskObject
{
    id<TLTaskList> ReturnValue = [TLTasksListGeneric createList];
    
    NSArray<id<TLTask>> * List = [[self taskList] allValues];
    for(int i = 0; i< List.count; i++)
    {
        [ReturnValue addTask:[List objectAtIndex:i]];
    }
    
    return ReturnValue;
}

-(id<TLTask>) createNewTaskWithName:(NSString* ) Name WithDeadLine:(id<KADate>) Deadline
{
    id<TLTask> ReturnValue =  [self.taskFactory createTaskWithName:Name WithDeadline:Deadline];
    ReturnValue.owner = self;
    return ReturnValue;
    
}

-(id<TLTask>) taskWithID:(NSString *) TaskId
{
    id<TLTask> ReturnValue = [self.taskList objectForKey:TaskId];
    
    if(ReturnValue == nil)
    {
        ReturnValue = self.NULL_TASK;
    }
    
    return ReturnValue;
}

-(id<TLTask>) taskWithIndex:(NSInteger)Index
{
    id<TLTask> ReturnValue = [self.tasks objectAtIndex:Index];
    
    return ReturnValue;
}

-(id<TLTask>) createNewTask
{
    id<TLTask> ReturnValue =  [self.taskFactory createNewTask];
    ReturnValue.owner = self;
    return ReturnValue;
}




@end
