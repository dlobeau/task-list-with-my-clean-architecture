//
//  TLDelegateGenericTaskFactory.h
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "TLDelegateTaskFactory.h"
NS_ASSUME_NONNULL_BEGIN

@interface TLDelegateGenericTaskFactory : NSObject<TLDelegateTaskFactory>

@end

NS_ASSUME_NONNULL_END
