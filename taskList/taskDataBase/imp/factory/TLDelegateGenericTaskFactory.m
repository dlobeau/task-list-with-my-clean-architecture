//
//  TLDelegateGenericTaskFactory.m
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLDelegateGenericTaskFactory.h"
#import "TLTaskGeneric.h"
#import "KADate.h"
#import "KAApplicationSerializableObjectImp.h"
#import "KACalendar.h"

@implementation TLDelegateGenericTaskFactory

-(id<TLTask>) createTaskWithName:(NSString *) name WithDeadline:(id<KADate>) Date;
{
    id<KAApplication> App = [KAApplicationSerializableObjectImp instance];
    TLTaskGeneric* ReturnValue = [[TLTaskGeneric alloc] initWithName:name WithDeadline:Date];
    ReturnValue.factoryDelegate = App.factory;
    return ReturnValue;
}

-(id<TLTask>) createNewTask
{
    id<TLTask> ReturnValue = nil;
    id<KAApplication> App = [KAApplicationSerializableObjectImp instance];
    id<KACalendar> Calendar = App.calendar;
    
    id<KADate> Date = [Calendar createCurrentDay];
    ReturnValue = [self createTaskWithName:@"Enter new task name" WithDeadline:Date];
    return ReturnValue;
}
@end
