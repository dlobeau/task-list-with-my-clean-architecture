//
//  TLTasksDataBase.h
//  taskList
//
//  Created by Didier Lobeau on 22/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KADomain.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TLTask;
@protocol KADate;
@protocol TLDelegateTaskFactory;


@protocol TLTasksDataBase <KADomain>

-(NSArray<id<TLTask>> *) tasks;
-(void) addTask:(id<TLTask>) Task;
-(void) removeTaskWithId:(NSString *) TaxId;
-(id<TLTask>) taskWithID:(NSString *) TaskId;
-(id<TLTask>) taskWithIndex:(NSInteger) Index;
-(NSInteger) tasksCount;
-(id<TLTask>) NULL_TASK;


-(id<TLTask>) createNewTaskWithName:(NSString* ) Name WithDeadLine:(id<KADate>) Deadline;

-(id<TLTask>) createNewTask;

-(id<TLDelegateTaskFactory>) taskFactory;
-(void) setTaskFactory:(id<TLDelegateTaskFactory>) taskFactory;

@end

NS_ASSUME_NONNULL_END
