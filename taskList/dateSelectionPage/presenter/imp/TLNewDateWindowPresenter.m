//
//  TLNewDateWindowPresenter.m
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLNewDateWindowPresenter.h"
#import "KADate.h"
#import "TLPicker.h"
#import "TLButton.h"

@interface TLNewDateWindowPresenter()

@property id<TLPicker> datePicker;
@property id<TLButton> saveButton;


@end

@implementation TLNewDateWindowPresenter

@synthesize datePicker = _datePicker;
@synthesize saveButton = _saveButton;

-(id) initWithLabel:(NSString *)label WithLabelIdentifier:(NSString *)ID WithObjectFamilyName:(NSString *)GroupId WithID:(NSString *)TypeId
{
    self = [super initWithLabel:label WithLabelIdentifier:ID WithObjectFamilyName:GroupId WithID:TypeId];
    self.testdelegate = [[TLNewDateWindowPresenterTest alloc] init];
    return self;
}

-(id<TLPicker>) datePicker
{
    if(_datePicker == nil)
    {
        _datePicker = (id<TLPicker>)[self getChildwithIdentifier:@"datePicker"];
    }
    NSAssert([_datePicker conformsToProtocol:@protocol(TLPicker)], @"_datePicker must be defined in TLNewDateWindowPresenter, check %@",self.attributeIdentity);
    return _datePicker;
}
-(void) setDatePicker:(id<TLPicker>)datePicker
{
    _datePicker = datePicker;
}

-(id<TLButton>) saveButton
{
    if(_saveButton == nil)
    {
        _saveButton = (id<TLButton>)[self getChildwithIdentifier:@"button"];
    }
    NSAssert([_saveButton conformsToProtocol:@protocol(TLButton)], @"saveButton must be defined in TLNewDateWindowPresenter, check %@",self.attributeIdentity);
    return _saveButton;
}
-(void) setSaveButton:(id<TLButton>)saveButton
{
    _saveButton = saveButton;
}

@end

@implementation TLNewDateWindowPresenterTest

-(void) expectSender: (id<TLNewDateWindow>) Sender conformToDomain:(id<KADate>) Domain
{
    NSAssert([Domain conformsToProtocol:@protocol(KADate)], @"TLNewDateWindowPresenterTest should be test with KADate data");
    
    NSAssert([Sender.data compareAttribute:Domain] == NSOrderedSame, @"TLNewDateWindow must be intialize with reference date: %@ instead of %@",Sender.data.label,Domain.label);
    [Sender.datePicker.testdelegate expectSender:Sender.datePicker conformToDomain:Domain];
   
    
}

@end
