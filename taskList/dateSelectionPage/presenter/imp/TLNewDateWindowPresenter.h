//
//  TLNewDateWindowPresenter.h
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAGenericPresenter.h"
#import "TLNewDateWindow.h"
#import "KAPresenterConformToDomain.h"
NS_ASSUME_NONNULL_BEGIN

@interface TLNewDateWindowPresenter : KAGenericPresenter<TLNewDateWindow>

@end

@interface TLNewDateWindowPresenterTest : NSObject<KAPresenterConformToDomain>

@end

NS_ASSUME_NONNULL_END
