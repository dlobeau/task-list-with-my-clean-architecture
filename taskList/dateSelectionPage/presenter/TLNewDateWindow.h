//
//  TLNewDateWindow.h
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAPresenter.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TLPicker;
@protocol TLButton;

@protocol TLNewDateWindow <KAPresenter>

-(id<TLPicker>) datePicker;
-(id<TLButton>) saveButton;

@end

NS_ASSUME_NONNULL_END
