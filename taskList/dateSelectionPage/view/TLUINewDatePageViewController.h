//
//  TLUINewDatePageViewController.h
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUIFacadeViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TLUINewDatePageViewController : TLUIFacadeViewController

@end

NS_ASSUME_NONNULL_END
