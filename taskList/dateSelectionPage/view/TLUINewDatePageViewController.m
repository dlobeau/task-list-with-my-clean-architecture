//
//  TLUINewDatePageViewController.m
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUINewDatePageViewController.h"
#import "TLUIDatePickerFacade.h"
#import "TLUISaveTaskButton.h"
#import "KAPresenter.h"

@interface TLUINewDatePageViewController ()
@property (weak, nonatomic) IBOutlet TLUIDatePickerFacade *datePicker;
@property (weak, nonatomic) IBOutlet TLUISaveTaskButton *saveButton;

@end

@implementation TLUINewDatePageViewController

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setContent];
}

-(BOOL) setContent
{
    BOOL ReturnValue =   [super setContent];
    [self.datePicker setContent];
    [self.saveButton setContent];
    
    return ReturnValue;
}

- (IBAction)clickOnSaveButton:(id)sender
{
    [self.datePicker validateUserChange];
    [self.interface addCommandOnPresenterModificationWithPresenterWithNewValue:self.datePicker.interface WithSender:self.interface];
    [self.interface executeCommandHierarchy];
    [self.interface validateChange];
    [self closeWindow];
}



@end
