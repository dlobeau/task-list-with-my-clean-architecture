//
//  TLTaskDescriptionWindowSaveButton.h
//  taskList
//
//  Created by Didier Lobeau on 29/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLButton.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TLTaskDescriptionWindowSaveButton <TLButton>

@end

NS_ASSUME_NONNULL_END
