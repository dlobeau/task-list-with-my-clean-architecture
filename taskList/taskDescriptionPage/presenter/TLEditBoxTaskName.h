//
//  TLEditBoxTaskName.h
//  taskList
//
//  Created by Didier Lobeau on 29/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAPresenter.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TLEditBoxTaskName <KAPresenter>

@end

NS_ASSUME_NONNULL_END
