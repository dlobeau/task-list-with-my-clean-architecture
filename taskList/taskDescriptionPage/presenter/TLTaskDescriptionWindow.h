//
//  TLTaskDetailsWindowPresenter.h
//  taskList
//
//  Created by Didier Lobeau on 22/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAPresenter.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TLEditBoxTaskName;
@protocol TLButton;

@protocol TLTaskDescriptionWindow <KAPresenter>

-(id<TLEditBoxTaskName>) editBoxName;
-(id<TLButton>) deadlineSelectionButton;
-(id<TLButton>) saveButton;

@end

NS_ASSUME_NONNULL_END
