//
//  TLTaskDescriptionWindowPresenter.h
//  taskList
//
//  Created by Didier Lobeau on 29/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAGenericPresenter.h"
#import "TLTaskDescriptionWindow.h"
#import "KAPresenterConformToDomain.h"

NS_ASSUME_NONNULL_BEGIN

@interface TLTaskDescriptionWindowPresenter : KAGenericPresenter<TLTaskDescriptionWindow>

@end


@interface TLTaskDescriptionWindowPresenterConformToDomain:NSObject< KAPresenterConformToDomain>


@end
NS_ASSUME_NONNULL_END
