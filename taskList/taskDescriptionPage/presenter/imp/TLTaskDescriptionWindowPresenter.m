//
//  TLTaskDescriptionWindowPresenter.m
//  taskList
//
//  Created by Didier Lobeau on 29/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLTaskDescriptionWindowPresenter.h"
#import "TLTask.h"
#import "TLEditBoxTaskName.h"
#import "KADate.h"
#import "TLButton.h"

@interface TLTaskDescriptionWindowPresenter()

@property id<TLEditBoxTaskName> editBoxName;
@property id<TLButton> saveButton;
@property id<TLButton> deadlineSelectionButton;

@end

@implementation TLTaskDescriptionWindowPresenter

@synthesize editBoxName = _editBoxName;
@synthesize saveButton = _saveButton;
@synthesize deadlineSelectionButton = _deadlineSelectionButton;

-(id) initWithLabel:(NSString *)label WithLabelIdentifier:(NSString *)ID WithObjectFamilyName:(NSString *)GroupId WithID:(NSString *)TypeId
{
    self = [super initWithLabel:label WithLabelIdentifier:ID WithObjectFamilyName:GroupId WithID:TypeId];
    self.testdelegate = [[TLTaskDescriptionWindowPresenterConformToDomain alloc] init];
    return self;
}

-(id<TLEditBoxTaskName>) editBoxName
{
    if(_editBoxName == nil)
    {
        _editBoxName = (id<TLEditBoxTaskName>)[self getChildwithIdentifier:@"editBoxWithName"];
    }
    NSAssert([_editBoxName conformsToProtocol:@protocol(TLEditBoxTaskName)], @"edit box is not defined in TLTaskDescriptionWindowPresenter, check %@",self.attributeIdentity);
    return _editBoxName;
    
}
-(void) setEditBoxName:(id<TLEditBoxTaskName>)editBoxName
{
    _editBoxName = editBoxName;
}

-(id<TLButton>) saveButton
{
    if(_saveButton == nil)
    {
        _saveButton = (id<TLButton>)[self getChildwithIdentifier:@"button"];
    }
    NSAssert([_saveButton conformsToProtocol:@protocol(TLButton)], @"button save is not defined in TLTaskDescriptionWindowPresenter, check %@",self.attributeIdentity);
    return _saveButton;
}
-(void) setSaveButton:(id<TLButton>)saveButton
{
    _saveButton = saveButton;
}

-(id<TLButton>) deadlineSelectionButton
{
    if(_deadlineSelectionButton == nil)
    {
        _deadlineSelectionButton = (id<TLButton>)[self getChildwithIdentifier:@"deadlineButton"];
    }
     NSAssert([_deadlineSelectionButton conformsToProtocol:@protocol(TLButton)], @"_deadlineSelectionButton is not defined in TLTaskDescriptionWindowPresenter, check %@",self.attributeIdentity);
    return _deadlineSelectionButton;
}
-(void) setDeadlineSelectionButton:(id<TLButton>)deadlineSelectionButton
{
    _deadlineSelectionButton = deadlineSelectionButton;
}
@end

@implementation TLTaskDescriptionWindowPresenterConformToDomain

-(void) expectSender: (id<TLTaskDescriptionWindow>) Sender conformToDomain:(id<TLTask>) Domain
{
    NSAssert([Domain conformsToProtocol:@protocol(TLTask)], @"TLTaskDescriptionWindowPresenterConformToDomain should be test with TLTask data");
    
    [Sender.editBoxName.testdelegate expectSender:Sender.editBoxName conformToText:Domain.name ];
    [Sender.deadlineSelectionButton.testdelegate expectSender:Sender.deadlineSelectionButton conformToText:Domain.deadline.completeString ];
    
}

@end


