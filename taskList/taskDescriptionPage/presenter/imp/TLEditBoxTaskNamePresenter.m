//
//  TLEditBoxTaskNamePresenter.m
//  taskList
//
//  Created by Didier Lobeau on 29/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLEditBoxTaskNamePresenter.h"
#import "KACreateUIWidgetEditBoxFactory.h"
@implementation TLEditBoxTaskNamePresenter

-(id) initWithLabel:(NSString *)label WithLabelIdentifier:(NSString *)ID WithObjectFamilyName:(NSString *)GroupId WithID:(NSString *)TypeId
{
    self = [super initWithLabel:label WithLabelIdentifier:ID WithObjectFamilyName:GroupId WithID:TypeId];
    self.testdelegate = [[TLEditBoxTaskNamePresenterConformToDomain alloc] init];
    self.viewFactoryDelegate = [[KACreateUIWidgetEditBoxFactory alloc] init];
    return self;
}


@end

@implementation TLEditBoxTaskNamePresenterConformToDomain

-(void)expectSender:(id<TLEditBoxTaskName>)Sender conformToDomain:(id<KADomain>)Domain
{
    
}

-(void) expectSender:(id<TLEditBoxTaskName>)Sender conformToText:(NSString *)ReferenceText
{
    NSString *TextUnderTest = [Sender getText];
    
    NSAssert([ReferenceText isEqual:TextUnderTest],@"%@ instead of %@, check %@",TextUnderTest,ReferenceText,Sender.attributeIdentity);
}

@end

