//
//  TLTaskDescriptionViewController.m
//  taskList
//
//  Created by Didier Lobeau on 30/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUITaskDescriptionViewController.h"
#import "TLTaskDescriptionWindow.h"
#import "TLUITextFieldFacade.h"
#import "TLUISaveTaskButton.h"
#import "TLUIStaticLabel.h"
#import "TLEditBoxTaskName.h"
#import "KAEvent.h"

@interface TLUITaskDescriptionViewController ()

@property (weak) id<TLTaskDescriptionWindow> interface;

@property (weak, nonatomic) IBOutlet TLUITextFieldFacade *editBox;
@property (weak, nonatomic) IBOutlet TLUISaveTaskButton *saveButton;
@property (weak, nonatomic) IBOutlet TLUIStaticLabel *editBoxLabel;
@property (weak, nonatomic) IBOutlet TLUIButtonFacade *deadlineButton;


@end

@implementation TLUITaskDescriptionViewController

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setContent];
}

-(BOOL) setContent
{
    BOOL ReturnValue =   [super setContent];
    [self.editBox setContent];
    [self.saveButton setContent];
    [self.editBoxLabel setContent];
    [self.deadlineButton setContent];
    return ReturnValue;
}

- (IBAction)deadLineButtonClickAction:(id<KAView>)sender
{
    id<KAEvent> E = [sender.interface event];
    id<KAPresenter> NewWindowPresenter = [E getDestinationWithOwner:sender.interface];
    id<KAView> NewWindow = [NewWindowPresenter createViewWithOwner:sender];
    [self.delegatePushNextView pushNextPage:NewWindow];
    
}

- (IBAction)saveButtonCLick:(id)sender
{
    [self.interface addCommandOnChildPresenterModification:self.editBox.interface WithSender:self.interface];
    [self.interface addCommandOnChildPresenterModification:self.deadlineButton.interface WithSender:self.interface];
    [self.interface executeCommandHierarchy];
    [self.interface validateChange];
    [self closeWindow];
}



@end
