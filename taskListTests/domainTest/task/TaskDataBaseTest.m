//
//  TaskDataBaseTest.m
//  taskListTests
//
//  Created by Didier Lobeau on 26/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TestEnvironmentSettings.h"
#import "TLTasksDataBase.h"
#import "KADate.h"
#import "TLTask.h"
#import "KAApplication.h"
#import "KACalendar.h"
#import "TLTaskList.h"

@interface TaskDataBaseTest : XCTestCase
{
    TestEnvironmentSettings *TestEnvironment;
    id<KAApplication> app;
    id<TLTasksDataBase> dataBase;
    id<KACalendar> calendar;
}
@end

@implementation TaskDataBaseTest

- (void)setUp
{
    TestEnvironment = [[TestEnvironmentSettings alloc] init];
    
    app = [TestEnvironment application];
    calendar = [TestEnvironment calendar];
}

- (void)tearDown {
    
}

- (void)testTaskDataBase_DatabaseSHouldBeintializedFromApplication_shouldBeAccessible
{
    dataBase = TestEnvironment.taskDataBase;
    XCTAssertTrue([dataBase conformsToProtocol:@protocol(TLTasksDataBase)]);
}

- (void)testTaskDataBase_TaskCreatedFromDataBase_sHouldNotNeNill
{
    dataBase = TestEnvironment.taskDataBase;
    NSString *strDate = @"2020-10-10";
    NSString *Name = @"Task test 1";
    
    id<KADate> Date = [calendar createDayWithString:strDate];
    id<TLTask> Task = [dataBase createNewTaskWithName:Name WithDeadLine:Date];
    XCTAssertTrue([Task conformsToProtocol:@protocol(TLTask)]);
    XCTAssertTrue([Name isEqual:Task.name]);
    XCTAssertTrue([Date isEqual:Task.deadline]);
}

- (void)testTaskDataBase_TaskInsertedInDataBase_shouldBeretriedvedFromRequest
{
    dataBase = TestEnvironment.taskDataBase;
    NSString *strDate = @"2020-10-10";
    NSString *Name = @"Task test 1";
    id<KADate> Date = [calendar createDayWithString:strDate];
    id<TLTask> Task = [dataBase createNewTaskWithName:Name WithDeadLine:Date];

    [Task validate];

    id<TLTask> ExtractedTask = [dataBase taskWithID:Task.ID];
    XCTAssertTrue(dataBase.NULL_TASK != ExtractedTask);
    XCTAssertTrue([Task compareAttribute:ExtractedTask] == NSOrderedSame);
}
    
- (void)testTaskDataBase_whenRequestTaskNotInserted_dataBAseSHouldREturnNoTask
{
    dataBase = TestEnvironment.taskDataBase;
  
    id<TLTask> ExtractedTask = [dataBase taskWithID:@"dummy ID"];
    XCTAssertNotNil(dataBase.NULL_TASK);
    XCTAssertTrue(dataBase.NULL_TASK  == ExtractedTask);
}

- (void)testTaskDataBase_AllTaskInsertedInDataBase_shouldBeretriedvedFromListRequest
{
    dataBase = TestEnvironment.taskDataBase;
    
    NSString *strDate = @"2020-10-10";
    NSString *Name = @"Task test 1";
    id<KADate> Date = [calendar createDayWithString:strDate];
    id<TLTask> Task1 = [dataBase createNewTaskWithName:Name WithDeadLine:Date];
    
    strDate = @"2020-12-10";
    Name = @"Task test 2";
    Date = [calendar createDayWithString:strDate];
    id<TLTask> Task2 = [dataBase createNewTaskWithName:Name WithDeadLine:Date];
    
    [Task1 validate];
    [Task2 validate];
    
    NSArray<id<TLTask>> * List = [dataBase tasks];
    
    XCTAssert([List count] == 2, @"%ld instead 2",[List count]);
    
    id<TLTask> ExtractedTask1 = [List objectAtIndex:0];
    id<TLTask> ExtractedTask2 = [List objectAtIndex:1];
    
    XCTAssertTrue([Task1 compareAttribute:ExtractedTask1] == NSOrderedSame);
    XCTAssertTrue([Task2 compareAttribute:ExtractedTask2] == NSOrderedSame);
}

- (void)testTaskDataBase_whenTaskREmoveFromDatabase_TaskShouldNotReapperFRomRequest
{
    dataBase = TestEnvironment.taskDataBase;
    
    NSString *strDate = @"2020-10-10";
    NSString *Name = @"Task test 1";
    id<KADate> Date = [calendar createDayWithString:strDate];
    id<TLTask> Task1 = [dataBase createNewTaskWithName:Name WithDeadLine:Date];
    
   
    [Task1 validate];
    [Task1 deleteItem];
    
    NSArray<id<TLTask>> * List = [dataBase tasks];
    
    XCTAssert([List count] == 0, @"%ld instead 0",[List count]);
    
    
}

- (void)testTaskDataBase_whenTaskREmoveFromDatabase_TaskModificationSHouldNotCreateDuplicationInDataBase
{
    dataBase = TestEnvironment.taskDataBase;
    
    NSString *strDate = @"2020-10-10";
    NSString *Name = @"Task test 1";
    id<KADate> Date = [calendar createDayWithString:strDate];
    id<TLTask> Task1 = [dataBase createNewTaskWithName:Name WithDeadLine:Date];
    
    
    [Task1 validate];
    
    [Task1 setName:@"new name"];
    
    [Task1 validate];
    
    NSArray<id<TLTask>> * List = [dataBase tasks];
    
    XCTAssert([List count] ==1, @"%ld instead 1",[List count]);
    
    
}

- (void)testTaskDataBase_AllTaskRequestFromDataBase_shouldBeListOfAlltAskInDataBase
{
    dataBase = TestEnvironment.taskDataBase;
    
    NSString *strDate = @"2020-10-10";
    NSString *Name = @"Task test 1";
    id<KADate> Date = [calendar createDayWithString:strDate];
    id<TLTask> Task1 = [dataBase createNewTaskWithName:Name WithDeadLine:Date];
    
    strDate = @"2020-12-10";
    Name = @"Task test 2";
    Date = [calendar createDayWithString:strDate];
    id<TLTask> Task2 = [dataBase createNewTaskWithName:Name WithDeadLine:Date];
    
    [Task1 validate];
    [Task2 validate];
    
    id<TLTaskList> ListObject = (id<TLTaskList>)[dataBase requestWithID:@"allTasks"];
    XCTAssertTrue([ListObject conformsToProtocol:@protocol(TLTaskList)]);
    
    NSArray<id<TLTask>> *List = ListObject.tasks;
    
    XCTAssert([List count] == 2, @"%ld instead 2",[List count]);
    
    id<TLTask> ExtractedTask1 = [List objectAtIndex:0];
    id<TLTask> ExtractedTask2 = [List objectAtIndex:1];
    
    XCTAssertTrue([Task1 compareAttribute:ExtractedTask1] == NSOrderedSame);
    XCTAssertTrue([Task2 compareAttribute:ExtractedTask2] == NSOrderedSame);
}

- (void)testTaskDataBase_whenRequestedDeadlineFromTask_shouldReturnDeadline
{
    dataBase = TestEnvironment.taskDataBase;
    
    NSString *strDate = @"2020-10-10";
    NSString *Name = @"Task test 1";
    id<KADate> Date = [calendar createDayWithString:strDate];
    id<TLTask> Task1 = [dataBase createNewTaskWithName:Name WithDeadLine:Date];
    
    id<KADate> ExtracedDeadline = ( id<KADate>) [Task1 requestWithID:@"deadline"];
    
    XCTAssertTrue([Date compareAttribute:ExtracedDeadline] == NSOrderedSame);
    
}

- (void)testTaskDataBase_whenRequestedNameFromTask_shouldReturnName
{
    dataBase = TestEnvironment.taskDataBase;
    
    NSString *strDate = @"2020-10-10";
    NSString *Name = @"Task test 1";
    id<KADate> Date = [calendar createDayWithString:strDate];
    id<TLTask> Task1 = [dataBase createNewTaskWithName:Name WithDeadLine:Date];
    
    id<KADomain> ExtracedName = ( id<KADate>) [Task1 requestWithID:@"name"];
    
    XCTAssertTrue([Name isEqual:ExtracedName.label],@"%@ instead of %@",Name, ExtracedName.label);
    
}



@end
