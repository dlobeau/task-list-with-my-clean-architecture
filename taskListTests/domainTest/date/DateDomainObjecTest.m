//
//  DateDomainObjecTest.m
//  taskListTests
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TestEnvironmentSettings.h"
#import "KACalendar.h"
#import "KADate.h"

@interface DateDomainObjecTest : XCTestCase
{
    TestEnvironmentSettings *TestEnvironment;
    id<KAApplication> app;
    
    id<KACalendar> calendar;
}
@end

@implementation DateDomainObjecTest

- (void)setUp
{
    TestEnvironment = [[TestEnvironmentSettings alloc] init];
    
    app = [TestEnvironment application];
    calendar = [TestEnvironment calendar];
}

- (void)tearDown
{
    
}

- (void)testDateDomainObjecTest_DateCrationFromString
{
    id<KADate> Date = [calendar createDayWithString:@"2020-10-10"];
    
    XCTAssertTrue([Date conformsToProtocol:@protocol(KADate)]);
    XCTAssertTrue([@"2020-10-10" isEqual:Date.label]);
}

- (void)testDateDomainObjecTest_whenDateID_2020_10_10_comlpleteStringSHouldBe10Ocobre2020
{
    id<KADate> Date = [calendar createDayWithString:@"2020-10-10"];
    
    NSString *Expected = @"10 October 2020";
    
    XCTAssertTrue([Expected isEqual:Date.completeString],@"%@ instead of %@",Date.completeString,Expected);
}

- (void)testDateDomainObjecTest_whenDateIsCUrrentDay_comlpleteStringSHouldBe10Ocobre2020
{
    id<KADate> Date = [calendar createCurrentDay];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.locale = [NSLocale localeWithLocaleIdentifier:[[NSLocale preferredLanguages] firstObject]];
    [dateFormat setDateFormat:@"dd MMMM YYYY"];
    NSString *Expected = [dateFormat stringFromDate:[NSDate date] ];
  
    
    XCTAssertTrue([Expected isEqual:Date.completeString],@"%@ instead of %@",Date.completeString,Expected);
}



@end
