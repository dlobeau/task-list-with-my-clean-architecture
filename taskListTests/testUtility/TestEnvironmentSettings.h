//
//  TestEnvironmentStettings.h
//  taskListTests
//
//  Created by Didier Lobeau on 26/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol  KAApplication;
@protocol   KACalendar;
@protocol KAPresenter;
@protocol TLTasksDataBase;
@interface TestEnvironmentSettings : NSObject

-(id<KAApplication>) application;
-(id<KACalendar>) calendar;
-(id<KAPresenter>) createWithContentsOfFileWithFileName:(NSString *) FileName;
-(id<TLTasksDataBase>) taskDataBase;
-(void) createDummyTasksInDataBase;

@end

NS_ASSUME_NONNULL_END
