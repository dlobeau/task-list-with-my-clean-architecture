//
//  TestEnvironmentStettings.m
//  taskListTests
//
//  Created by Didier Lobeau on 26/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TestEnvironmentSettings.h"
#import "KAApplicationSerializableObjectImp.h"
#import "TLTasksListApplication.h"
#import "KACalendarGeneric.h"
#import "TLTask.h"
#import "TLTasksDataBase.h"
#import "KASerializeObjectFactory.h"

@interface TestEnvironmentSettings()

@property id<TLTasksListApplication> application;

@end

@implementation TestEnvironmentSettings

@synthesize application = _application;

-(id<KACalendar>) calendar
{
    return self.application.calendar;
}

-(id<KAApplication>) application
{
    if(_application == nil)
    {
        NSArray<NSString *> *InjectionList = @[@"presenter_items",@"query_items",@"domain_items"];
    
        [KAApplicationSerializableObjectImp createInstanceWithFileName:@"ApplicationMainWindowMainApp" WithInjectionDependencyFileNameList:InjectionList];
        _application = (id<TLTasksListApplication>)[KAApplicationSerializableObjectImp instance];;
        [_application initializaDataBase];
    }
    return _application;
}
-(void) setApplication:(id<TLTasksListApplication>)application
{
    _application = application;
}

-(id<TLTasksDataBase>) taskDataBase
{
    return [(id<TLTasksListApplication>)self.application taskDataBase];
}

-(void) createDummyTasksInDataBase
{
    NSString *strDate = @"2020-10-10";
    NSString *Name = @"Task test 1";
    id<KADate> Date = [self.calendar createDayWithString:strDate];
    id<TLTask> Task1 = [self.taskDataBase createNewTaskWithName:Name WithDeadLine:Date];
    
    strDate = @"2020-12-10";
    Name = @"Task test 2";
    Date = [self.calendar createDayWithString:strDate];
    id<TLTask> Task2 = [self.taskDataBase createNewTaskWithName:Name WithDeadLine:Date];
    
    [Task1 validate];
    [Task2 validate];
}

-(id<KAPresenter>) createWithContentsOfFileWithFileName:(NSString *) FileName
{
    return [self.application.factory createWithContentsOfFileWithFileName:FileName];
}

@end
