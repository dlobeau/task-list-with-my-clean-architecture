//
//  taskListPagePresenterTest.m
//  taskListTests
//
//  Created by Didier Lobeau on 26/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TestEnvironmentSettings.h"
#import "TLTaskListWindowWithHeader.h"
#import "TLTaskDescriptionWindow.h"
#import "KAApplication.h"
#import "TLTasksDataBase.h"
#import "KAPresenterConformToDomain.h"
#import "TLTask.h"
#import "KACalendar.h"
#import "TLTaskListTable.h"
#import "TLTaskDescriptionWindow.h"
#import "TLTaskListTableCell.h"
#import "KACheckBox.h"

@interface taskListPagePresenterTest : XCTestCase
{
    TestEnvironmentSettings *TestEnvironment;
    id<KAApplication> app;
    id<TLTaskListWindowWithHeader> taskWindow;
    id<TLTasksDataBase> dataBase;
    id<KACalendar> calendar;
}
@end

@implementation taskListPagePresenterTest

- (void)setUp
{
    TestEnvironment = [[TestEnvironmentSettings alloc] init];
    
    app = [TestEnvironment application];
    dataBase = [TestEnvironment taskDataBase];
    calendar = [TestEnvironment calendar];
}

- (void)tearDown
{
    
}

- (void)testtaskListPagePresenter_pageSHouldBeIntilizedFromApplication
{
    taskWindow = (id<TLTaskListWindowWithHeader>)[app window];
    
    XCTAssertTrue([taskWindow conformsToProtocol:@protocol(TLTaskListWindowWithHeader)]);
}

- (void)testtaskListPagePresenter_when2TaskInDataBase_tableShouldContain2Cell
{
    [TestEnvironment createDummyTasksInDataBase];
    
    taskWindow = (id<TLTaskListWindowWithHeader>)[app window];
    [taskWindow initializeWithData];
    id<KASection> Section = [taskWindow.table sectionAtIndex:0];
    
    NSInteger CellCOunt = [[Section cells] count];
    
    
    XCTAssertTrue (CellCOunt == 2, @"%ld instead of 2",Section.cells.count);
}

- (void)testtaskListPagePresenter_pageSHouldBeConformTotaskDataBase
{
    [TestEnvironment createDummyTasksInDataBase];
    
    taskWindow = (id<TLTaskListWindowWithHeader>)[app window];
    [taskWindow initializeWithData];
    [taskWindow.testdelegate expectSender:taskWindow conformToDomain:dataBase];
}

- (void)testtaskListPagePresenter_WhenRequestCreationTaskPAgeDisplay_PresenterSHouldBeAccessible
{
    taskWindow = (id<TLTaskListWindowWithHeader>)[app window];
    id<TLTaskDescriptionWindow> NewTaskPage = [taskWindow taskCreationPage];
    XCTAssertNotNil(NewTaskPage);
    
    id<TLTask> NewTask = [dataBase createNewTask];
    [NewTaskPage.testdelegate expectSender:NewTaskPage conformToDomain:NewTask];
}

- (void)testtaskListPagePresenter_WhenTaskisNotCompleteAndCellIsSelected_TaskShouldBeMarkAsCompleteInDataBase
{
    [TestEnvironment createDummyTasksInDataBase];
    taskWindow = (id<TLTaskListWindowWithHeader>)[app window];
    id<KASection> Section = [taskWindow.table sectionAtIndex:0];
    id<TLTaskListTableCell> Cell = (id<TLTaskListTableCell>)[Section cellAtIndex:0];
    
    id<TLTask> Task = [dataBase taskWithID:Cell.ID];
    XCTAssertTrue(!Task.status.value);
    
    [Cell.taskStatusCheckBox addCommandOnPresenterModificationWithBoolean:YES WithSender:Cell.taskStatusCheckBox];
    [Cell.taskStatusCheckBox executeCommandHierarchy];
    
    [Cell addCommandOnChildPresenterModification:Cell.taskStatusCheckBox WithSender:Cell];
    [Cell executeCommandHierarchy];
     
    Task = [dataBase taskWithID:Cell.ID];
    XCTAssertTrue(Task.status.value);
    
}

- (void)testtaskListPagePresenter_WhenTaskLoadingTaskEditionPage_LoadedPageMustBeInitializedWithSelectedTask
{
    [TestEnvironment createDummyTasksInDataBase];
    taskWindow = (id<TLTaskListWindowWithHeader>)[app window];
    id<KASection> Section = [taskWindow.table sectionAtIndex:0];
    id<TLTaskListTableCell> Cell = (id<TLTaskListTableCell>)[Section cellAtIndex:0];
    
    id<TLTask> Task = [dataBase taskWithID:Cell.ID];
    id<TLTaskDescriptionWindow> Window = [Cell editTaskWindow];
     XCTAssertNotNil(Window);
    [Window.testdelegate expectSender:Window conformToDomain:Task];
    
}


@end
