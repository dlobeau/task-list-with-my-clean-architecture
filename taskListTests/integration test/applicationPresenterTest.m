//
//  applicationPresenterTest.m
//  taskListTests
//
//  Created by Didier Lobeau on 26/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TestEnvironmentSettings.h"

#import "TLTasksListApplication.h"


@interface applicationPresenterTest : XCTestCase
{
    TestEnvironmentSettings *TestEnvironment;
    id<TLTasksListApplication> app;
}
@end

@implementation applicationPresenterTest

- (void)setUp
{
    TestEnvironment = [[TestEnvironmentSettings alloc] init];
    
     app =(id<TLTasksListApplication>)[TestEnvironment application];
}

- (void)tearDown
{
}

- (void)testApplicationInitialization
{
    NSString *Reference = @"Application main file";
    NSString *UnderTest = app.label;
    
    XCTAssertTrue([Reference isEqual:UnderTest], @"%@ instead of %@",UnderTest,Reference);
}

- (void)testApplicationInitialization_DataBaseExtraction
{

    XCTAssertNotNil(app.taskDataBase);
}



@end
