//
//  taskCreationPagePresenterTest.m
//  taskListTests
//
//  Created by Didier Lobeau on 29/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "TestEnvironmentSettings.h"
#import "KAApplication.h"
#import "TLTasksDataBase.h"
#import "KAPresenterConformToDomain.h"
#import "TLTask.h"
#import "KACalendar.h"
#import "TLTaskDescriptionWindow.h"

#import "TLTaskDescriptionWindowSaveButton.h"
#import "KAEvent.h"
#import "KACommand.h"
#import "KACommandFactory.h"
#import "TLEditBoxTaskName.h"
#import "TLNewDateWindow.h"
#import "TLPicker.h"
#import "KADate.h"

@interface taskCreationPagePresenterTest : XCTestCase
{
    TestEnvironmentSettings *TestEnvironment;
    id<KAApplication> app;
    id<TLTaskDescriptionWindow> newTaskWindow;
    id<TLTasksDataBase> dataBase;
    id<KACalendar> calendar;
    id<TLTask> newTask;
}
@end

@implementation taskCreationPagePresenterTest

- (void)setUp
{
    TestEnvironment = [[TestEnvironmentSettings alloc] init];
    app = [TestEnvironment application];
    
    dataBase = [TestEnvironment taskDataBase];
    calendar = [TestEnvironment calendar];
    newTask = [dataBase createNewTask];
    
    newTaskWindow = (id<TLTaskDescriptionWindow>)[TestEnvironment createWithContentsOfFileWithFileName:@"taskDetailPage"];
    
    //data binding
    newTaskWindow.data = newTask;
}

- (void)tearDown
{
}

- (void)testTaskCreationPagePresenterTest_TestFileInitialization
{
    XCTAssertNotNil(newTaskWindow);
    
    [newTaskWindow.testdelegate expectSender:newTaskWindow conformToDomain:newTask];
}

- (void)testTaskCreationPagePresenterTest_onSaveActionTriggered_NewTaskSHouldBeAdded
{
    id<KAEvent> SaveEvent =  [newTaskWindow.saveButton event];
    
    [SaveEvent doEventActionWithSender:newTaskWindow.saveButton];
    
    XCTAssertEqual(dataBase.tasks.count, 1);
}

- (void)testTaskCreationPagePresenterTest_onModifyTaskNameAndSave_NewTaskSHouldBeAddedWithNewName
{
    NSString * NewTaskName = @"Finish wiki";
    
    id<TLEditBoxTaskName> EditBox = [newTaskWindow editBoxName];
    [EditBox addCommandOnLabelModificationWithNewLabel:NewTaskName WithSender:EditBox];
    [EditBox executeCommandHierarchy];
    
    [newTaskWindow addCommandOnChildPresenterModification:EditBox WithSender:newTaskWindow];
    [newTaskWindow executeCommandHierarchy];
    
    id<KAEvent> SaveEvent =  [newTaskWindow.saveButton event];
    [SaveEvent doEventActionWithSender:newTaskWindow.saveButton];
    
    id<TLTask> NewTask = [dataBase taskWithIndex:0];
    
    XCTAssertTrue([NewTaskName isEqual:NewTask.name], @"%@ instead of %@",NewTask.name,NewTaskName);
}

- (void)testTaskCreationPagePresenterTest_onModifyTaskDateFromChildDateSelectionWindow_TaskDescriptionPageShouldBeUPgradedWithNewDate
{
    id<KAEvent> DateModificationEvent =  [newTaskWindow.deadlineSelectionButton event];
   id<TLNewDateWindow> NewDateWindow =(id<TLNewDateWindow>) [DateModificationEvent getDestinationWithOwner:newTaskWindow.deadlineSelectionButton];
    
    id<KADate> DateReference = [calendar createDayWithString:@"2020-10-10"];
    [self ModifyDateSelectionWindow:NewDateWindow WithNewDate:DateReference];
    
    [newTaskWindow addCommandOnChildPresenterModification:newTaskWindow.deadlineSelectionButton WithSender:newTaskWindow];
    [newTaskWindow executeCommandHierarchy];
    
    id<KAEvent> SaveEvent =  [newTaskWindow.saveButton event];
    [SaveEvent doEventActionWithSender:newTaskWindow.saveButton];
    
    
    id<TLTask> NewTask = [dataBase taskWithIndex:0];
    
    XCTAssertTrue([DateReference compareAttribute:NewTask.deadline] == NSOrderedSame, @"%@ instead of %@",DateReference.label,NewTask.deadline.label);
}

- (void)testTaskCreationPagePresenterTest_onModifyTaskDateFromChildDateSelectionWindow_NewTaskCreatedShouldBeSetWithNewDate
{
    id<KAEvent> DateModificationEvent =  [newTaskWindow.deadlineSelectionButton event];
    id<TLNewDateWindow> NewDateWindow =(id<TLNewDateWindow>) [DateModificationEvent getDestinationWithOwner:newTaskWindow.deadlineSelectionButton];
    
    id<KADate> DateReference = [calendar createDayWithString:@"2020-10-10"];
    [self ModifyDateSelectionWindow:NewDateWindow WithNewDate:DateReference];
    
    id<KAEvent> SaveEvent =  [newTaskWindow.saveButton event];
    [SaveEvent doEventActionWithSender:newTaskWindow.saveButton];
    
    [newTaskWindow.deadlineSelectionButton.testdelegate expectSender:newTaskWindow.deadlineSelectionButton conformToText:DateReference.completeString];
}


-(void) ModifyDateSelectionWindow:(id<TLNewDateWindow>) Window WithNewDate:(id<KADate>) NewDate
{
    id<TLPicker> PickerDate = [Window datePicker];
    [PickerDate addCommandOnPresenterModificationWithNewDate:NewDate.getDateObject WithSender:PickerDate];
    [PickerDate executeCommandHierarchy];
    
    [Window addCommandOnPresenterModificationWithPresenterWithNewValue:PickerDate WithSender:Window];
    [Window executeCommandHierarchy];
    
    id<KAEvent> SaveEvent =  [Window.saveButton event];
    [SaveEvent doEventActionWithSender:Window.saveButton];
}



@end
