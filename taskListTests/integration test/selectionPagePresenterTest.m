//
//  selectionPagePresenterTest.m
//  taskListTests
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TLNewDateWindow.h"
#import "KAApplication.h"
#import "KACalendar.h"
#import "TestEnvironmentSettings.h"
#import "KADate.h"
#import "KAPresenterConformToDomain.h"
#import "TLPicker.h"
#import "KAEvent.h"
#import "TLButton.h"

@interface selectionPagePresenterTest : XCTestCase
{
    TestEnvironmentSettings *TestEnvironment;
    id<TLNewDateWindow> newDateWindow;
    id<KACalendar> calendar;
    
}
@end

@implementation selectionPagePresenterTest

- (void)setUp
{
    TestEnvironment = [[TestEnvironmentSettings alloc] init];
    
    calendar = [TestEnvironment calendar];
    
    newDateWindow = ( id<TLNewDateWindow>)[TestEnvironment createWithContentsOfFileWithFileName:@"newDateSelectionPage"];
}

- (void)tearDown
{
}

- (void)testselectionPagePresenterTest_WhenIntiallizationWithDate_PickerMustBeIntiliazedWithSameDate
{
    id<KADate> Date = [calendar createDayWithString:@"2020-10-10"];
    newDateWindow.data = Date;
    
    [newDateWindow.testdelegate expectSender:newDateWindow conformToDomain:Date];
}

- (void)testselectionPagePresenterTest_WhenPickerDateChangeAndPageValidation_dateWindowDataMustChageToNewSelectedDate
{
    id<KADate> Date = [calendar createDayWithString:@"2020-10-10"];
    newDateWindow.data = Date;
    
    id<KADate> ReferenceDate = [calendar createDayWithString:@"2020-12-10"];
    
    id<TLPicker> PickerDate = [newDateWindow datePicker];
    [PickerDate addCommandOnPresenterModificationWithNewDate:ReferenceDate.getDateObject WithSender:PickerDate];
    [PickerDate executeCommandHierarchy];
    
    [newDateWindow addCommandOnPresenterModificationWithPresenterWithNewValue:PickerDate WithSender:newDateWindow];
    [newDateWindow executeCommandHierarchy];
    
    id<KAEvent> SaveEvent =  [newDateWindow.saveButton event];
    [SaveEvent doEventActionWithSender:newDateWindow.saveButton];
    
    [newDateWindow.testdelegate expectSender:newDateWindow conformToDomain:ReferenceDate];
}



@end
