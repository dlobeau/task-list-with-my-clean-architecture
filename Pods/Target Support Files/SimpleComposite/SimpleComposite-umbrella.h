#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "KAComparable.h"
#import "KACompleteItemIdentification.h"
#import "KAGeneralLink.h"
#import "KAIterator.h"
#import "KASerializable.h"
#import "KATreeNavigable.h"
#import "KASerializeObjectLink.h"
#import "KASeriazableObject.h"
#import "KASeriazableObjectTable.h"
#import "KATableChildsManagementDelegate.h"
#import "SimpleComposite.h"
#import "KASerializeObjectFactory.h"
#import "KASerializeObjectFactoryImp.h"
#import "KASeriazableObjectImp.h"
#import "KASeriazableObjectImpLink.h"
#import "KASeriazableObjectTableImp.h"
#import "KATableFifoChildsManagement.h"
#import "KADcfArrayNode.h"
#import "KADcfDictionnaryNode.h"
#import "KADcfFileNode.h"
#import "KADcfGenericNode.h"
#import "KADcfNode.h"
#import "KADcfNodeFactory.h"
#import "NSArray+KASerializableObject.h"
#import "NSDictionary+KASeriazableObject.h"
#import "KAAttributeStreamParser.h"
#import "KAAttributeStreamParserPlistFile.h"
#import "KAAttributeStreamXMLParser.h"
#import "KASerializableObjectToFile.h"
#import "KABasicFile.h"
#import "KADataBaseFile.h"
#import "KAFile.h"
#import "NSString+KASeriazableObject.h"

FOUNDATION_EXPORT double SimpleCompositeVersionNumber;
FOUNDATION_EXPORT const unsigned char SimpleCompositeVersionString[];

