//
//  KAApplicationSerializableObjectImp.m
//  kakebo
//
//  Created by Didier Lobeau on 07/02/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import "KAApplicationSerializableObjectImp.h"

#import "KABasicFile.h"
#import "KASerializeObjectFactory.h"
#import "KASerializeObjectFactoryImp.h"
#import "KABasicFile.h"
#import "KADomain.h"
#import "KACalendarGeneric.h"



//presenterTag: MainApplication
@interface KAApplicationSerializableObjectImp()


@property id<KACalendar> calendar;
@property id<KASerializeObjectFactory> factory;
@property NSDictionary<NSString *,NSString*> *widgetTag;


@end

//MainApplication injected
@implementation KAApplicationSerializableObjectImp

@synthesize calendar = _calendar;

static NSBundle *bundle;

+(id<KAApplication>) instance
{
    static KAApplicationSerializableObjectImp *myInstance = nil;
    
    NSBundle *mainBundle = [KAApplicationSerializableObjectImp getBundle];
    NSDictionary *info = [mainBundle infoDictionary];
    
    myInstance = [info objectForKey:@"MainApplication"];
     NSAssert(myInstance != nil, @"Call createInstanceWithFileName before calling application instance"  );
    
    return myInstance;
}

+(NSBundle *) getBundle
{
    if(bundle == nil)
    {
        bundle = [NSBundle mainBundle];;
    }
    return bundle;
}


+(id<KAApplication>) createInstanceWithFileName:(NSString *) FileName
{
    
    NSArray<NSString *> *InjectionList = @[@"presenter_items",@"query_items",@"domain_items"];
    
    id<KAApplication> app = [self createInstanceWithFileName:FileName WithInjectionDependencyFileNameList:InjectionList];
    
    [app initializaDataBase];
    id<KAPresenter> Window = [app window];
    [Window initializeWithData];
    return app;
}

-(id) createObjectFromFamilyType:(NSString *)FamilyType
{
    return [self.factory createObjectFromFamilyType:FamilyType];
}

+(id<KAApplication>) createInstanceWithFileName:(NSString *) FileName WithInjectionDependencyFileNameList:(NSArray<NSString *> *)injectionDependencyFileNameList
{
    NSBundle *mainBundle = [KAApplicationSerializableObjectImp getBundle];
    
    id<KAFile> File = [[KABasicFile alloc] initWithFileName:FileName WithBundle:mainBundle];
    
    NSAssert([File isFileExist], @"Main application file: %@ should doesn't exist",FileName);
    
    id<KASerializeObjectFactory> Factory = [[KASerializeObjectFactoryImp alloc] initWithInjectionDependencyFileNameList:injectionDependencyFileNameList WithBundle:mainBundle];
    
    id<KAApplication> myInstance = [Factory createWithContentsOfFile:File];
    myInstance.factory = Factory;
    
    [[mainBundle infoDictionary] setValue:myInstance forKey:@"MainApplication"];
    
    return myInstance;
}


-(void) start
{
    [self initializaDataBase];
}

-(void) initializaDataBase
{
    
}

-(id<KAPresenter>) window
{
    return [[self childInterface] objectAtIndex:0];
}

-(id<KACalendar>) calendar
{
    if(_calendar == nil)
    {
        _calendar = [[KACalendarGeneric alloc] init];
    }
    return _calendar;
    
}
-(void) setCalendar:(id<KACalendar>)calendar
{
    _calendar = calendar;
}

-(id<KADomain>) datBaseContener
{
    return (id<KADomain>)[self getChildwithTypeId:@"data"];
}

-(NSDictionary *) getWidgetTagList
{
    if( self.widgetTag == nil)
    {
        
        self.widgetTag = [self.factory injectionFileContentWithFileName:@"UI_items"];
    }
    
     return self.widgetTag;
}

@end
