//
//  KAApplicationSerializableObjectImp.h
//  kakebo
//
//  Created by Didier Lobeau on 07/02/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import "KAGenericPresenter.h"
#import "KAApplication.h"



@interface KAApplicationSerializableObjectImp : KAGenericPresenter<KAApplication>

+(id<KAApplication>) instance;

+(id<KAApplication>) createInstanceWithFileName:(NSString *) FileName WithInjectionDependencyFileNameList:(NSArray<NSString *> *)injectionDependencyFileNameList;
+(id<KAApplication>) createInstanceWithFileName:(NSString *) FileName ;

-(id<KADomain>) datBaseContener;
@end
