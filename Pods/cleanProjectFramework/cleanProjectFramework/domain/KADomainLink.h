//
//  KALink.h
//  kakebo
//
//  Created by Didier Lobeau on 18/07/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KADomain.h"
#import "KASerializeObjectLink.h"
#import "KAGeneralLink.h"
@protocol KADomainLink <KADomain,KAGeneralLink>

-(id<KADomain>) source;
-(void) setSource:(id<KADomain>) Source;

@end
