//
//  KAQueryOperator.h
//  kakebo
//
//  Created by Didier Lobeau on 09/08/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//



@protocol KADomain;

@protocol KAQueryBusinessUseCase

-(id<KADomain>) executeWithRepository:(id<KADomain>) Repository WithValue:(id<KADomain>) Value;

@end
