//
//  KAQueryOperator.h
//  kakebo
//
//  Created by Didier Lobeau on 09/08/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KADomain;

@protocol KAQueryOperator <NSObject>

-(id<KADomain>) executeWithRepository:(id<KADomain>) Repository WithValue:(id<KADomain>) Value;

@end
