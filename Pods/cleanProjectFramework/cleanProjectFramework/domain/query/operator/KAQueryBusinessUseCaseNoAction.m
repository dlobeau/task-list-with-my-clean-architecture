//
//  KAQueryOperatorNoAction.m
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAQueryBusinessUseCaseNoAction.h"

@implementation KAQueryBusinessUseCaseNoAction

-(id<KADomain>) executeWithRepository:(id<KADomain>) Repository WithValue:(id<KADomain>) Value
{
    return Repository;
}

@end
