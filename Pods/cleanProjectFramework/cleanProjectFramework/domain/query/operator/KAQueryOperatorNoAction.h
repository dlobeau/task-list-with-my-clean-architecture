//
//  KAQueryOperatorNoAction.h
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KASeriazableObjectTableImp.h"
#import "KAQueryOperator.h"
NS_ASSUME_NONNULL_BEGIN

@interface KAQueryOperatorNoAction : KASeriazableObjectTableImp<KAQueryOperator>

@end

NS_ASSUME_NONNULL_END
