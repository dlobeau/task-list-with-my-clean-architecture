//
//  KABooleanAttribute.m
//  kakebo
//
//  Created by Didier Lobeau on 20/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KABooleanAttribute.h"


@implementation KABooleanAttribute

static NSString *TrueTag = @"YES";
static NSString *FalseTag = @"NO";
static NSString *BoolTypeTag = @"Bool";

+(NSString *) getTypeTag
{
    return BoolTypeTag;
}

-(id) initWithBoolValue:(BOOL ) Value WithTypeId:(NSString * )TypeId
{
    NSString *strValue = [FalseTag copy];
    
    if( Value)
    {
        strValue = [TrueTag copy];
    }
    return [[KABooleanAttribute alloc] initWithLabel:strValue WithLabelIdentifier:strValue WithObjectFamilyName:BoolTypeTag WithID:TypeId];
}

-(id) initWithBoolValue:(BOOL ) Value
{
     return [[KABooleanAttribute alloc] initWithBoolValue:Value WithTypeId:[KABooleanAttribute getTypeTag]];
}

-(BOOL) value
{
    BOOL ReturnValue = [self.labelIdentifier isEqual:TrueTag];
    
    return ReturnValue;
}

-(void) setValue:(BOOL) NewValue
{
    if(NewValue)
    {
        self.labelIdentifier = [TrueTag copy];
        
    }
    else
    {
        self.labelIdentifier = [FalseTag copy];
    }
    self.label = self.labelIdentifier;
    
}



@end
