//
//  KAGenericDomain.h
//  kakebo
//
//  Created by Didier Lobeau on 02/02/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//





#import "KADomain.h"
#import "KASeriazableObjectTableImp.h"
@protocol KARepository;

@interface KAGenericDomain : KASeriazableObjectTableImp <KADomain>



+(id) createGenericObjectWithLabel:(NSString *)Label;

@end
