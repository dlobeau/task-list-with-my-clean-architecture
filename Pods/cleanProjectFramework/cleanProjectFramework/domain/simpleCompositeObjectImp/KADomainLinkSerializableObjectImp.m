//
//  KADomainLinkSerializableObjectImp.m
//  taskList
//
//  Created by Didier Lobeau on 29/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KADomainLinkSerializableObjectImp.h"
#import "KADomain.h"
#import "KASerializeObjectFactoryImp.h"
#import "NSString+KASeriazableObject.h"
#import "NSString+KASeriazableObject.h"

@implementation KADomainLinkSerializableObjectImp

+(NSString *) getTypeTag
{
    return @"DomainLink";
}

+(KASeriazableObjectImpLink *) createFromSource:(id<KADomain,KASeriazableObject>) Source WithID:(NSString *) ID
{
    NSAssert([Source conformsToProtocol:@protocol(KADomain)], @"on doaminLink creation, source should be of domain type");
    
    KASeriazableObjectImpLink * ReturnValue = nil;
    
    NSAssert(Source != self, @"Can't create link on %@ from self",[Source attributeIdentity]);
    
    NSString *LinkCompletePath = [Source path];
    
    KASerializeObjectFactoryImp *Factory = [[KASerializeObjectFactoryImp alloc] init];
    
    ReturnValue = (KASeriazableObjectImpLink *)[Factory createAttributeFromLabel:LinkCompletePath WithID:LinkCompletePath WithGRoupId:[KADomainLinkSerializableObjectImp getTypeTag] WithTypeId:ID ];
    
    ReturnValue.sourceObject = Source;
    
    while((ReturnValue.sourceObject !=nil) &&
          [ReturnValue.sourceObject conformsToProtocol:@protocol(KASerializeObjectLink)])
    {
        ReturnValue.sourceObject = [(id<KASerializeObjectLink>)ReturnValue.sourceObject source];
    }
    
    
    return ReturnValue;
}

-(id<KADomain>) source
{
    return (id<KADomain>)[super source];
}



-(BOOL) validate
{
    return [self.source validate];
}
-(void) deleteItem
{
    [self.source deleteItem];
}

-(id<KADomainLink>) link
{
    return (id<KADomainLink>)[self cloneObject];
}

-(BOOL) isLink
{
    return YES;
}

-(BOOL) emptyChildList
{
    return [self.sourceObject emptyChildList];
}

-(id<KARepository>) delegateRepository
{
    return [self.source delegateRepository];
}
-(void) setDelegateRepository:(id<KARepository>)delegateRepository
{
    [self.source setDelegateRepository:delegateRepository];
}

-(NSArray<id<KADomain>> *) childs
{
    return self.source.childs;
}

-(id<KADomain>) requestWithID:(NSString *) RequestID
{
    return [self.source requestWithID:RequestID];
}

-(void) modifyChildDomainWithId:(NSString *)Id WithValue:(id<KADomain>)NewValue
{
    [self.source modifyChildDomainWithId:Id WithValue:NewValue];
}
@end
