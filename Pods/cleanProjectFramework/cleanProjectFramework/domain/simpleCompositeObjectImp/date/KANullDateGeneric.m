//
//  KANullDateGeneric.m
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KANullDateGeneric.h"

@implementation KANullDateGeneric

static NSString *NULL_DATE = @"NULL_DATE";

-(id) init
{
    self = [super initWithLabel:NULL_DATE WithLabelIdentifier:NULL_DATE WithObjectFamilyName:NULL_DATE WithID:NULL_DATE];
    
    return self;
}

-(NSDate *) getDateObject;
{
    return nil;
}

-(void) setLabel:(NSString *)Label
{
   
    
}

- (NSComparisonResult ) compareAttribute:(id<KADate>) AttributeToBeCompared
{
    NSComparisonResult Result = NSOrderedDescending;
    
    if([NULL_DATE isEqual:AttributeToBeCompared.label])
    {
        Result = NSOrderedSame;
    }
    
    
    return Result;
}


-(NSString *) completeString
{
    return NULL_DATE;;
}


-(NSString *) dayInMonth
{
    return NULL_DATE;
}


@end
