//
//  KADateAttribute.m
//  kakebo
//
//  Created by Didier Lobeau on 11/08/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KADateAttribute.h"
#import "KADomain.h"
#import "KACalendar.h"
#import "KANullDateGeneric.h"
@interface KADateAttribute()

@property (weak) id<KACalendar> calendar;

@end


@implementation KADateAttribute

static NSString *strDateTagId = @"Date";



-(id) NULL_DATE
{
    return [[KANullDateGeneric alloc] init];
}

+(NSString *) getTypeTag
{
    return strDateTagId;
}

-(id) initWithDateString:(NSString *) DateString WithTypeId:(NSString *)TypeId {
    return [super initWithLabel:DateString WithLabelIdentifier:DateString WithObjectFamilyName:strDateTagId WithID:TypeId   ];
}


-(id) initWithStandardDateObject:(NSDate *) DateObject WithTypeId:(NSString *)TypeId
{
    NSString * strDate =[[self.calendar getDateFormat] stringFromDate:DateObject];
    return [super initWithLabel:strDate WithLabelIdentifier:strDate WithObjectFamilyName:strDateTagId WithID:strDateTagId];
}

-(NSDate *) getDateObject;
{
    NSString *strDate = self.labelIdentifier;
    
    NSDateFormatter *format = [self.calendar getDateFormat];
        
    return [format dateFromString:strDate];
}

-(void) setLabel:(NSString *)Label
{
    [super setLabel:Label.copy];
    [super setLabelIdentifier:Label.copy];
    
}

- (NSComparisonResult ) compareAttribute:(id<KADate>) AttributeToBeCompared
{
    NSAssert([AttributeToBeCompared conformsToProtocol:@protocol(KADate)], @"Date must be compared with Date");
    
    NSComparisonResult Result = [self.getDateObject compare:AttributeToBeCompared.getDateObject];
        
    
    return Result;
}


-(NSString *) completeString
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.locale = [NSLocale localeWithLocaleIdentifier:[[NSLocale preferredLanguages] firstObject]];
    [dateFormat setDateFormat:@"dd MMMM YYYY"];
    
    return [dateFormat stringFromDate:self.getDateObject];;
}


-(NSString *) dayInMonth
{
    NSString * ReturnValue  = [self.labelIdentifier substringWithRange:NSMakeRange(8, 2)];
    return ReturnValue;
}


-(id<KADomain>) requestWithID:(NSString *)RequestID
{
    id<KADomain> ReturnValue = [super requestWithID:RequestID];
    
     if ([RequestID isEqual:@"completeString"])
    {
        ReturnValue = [KAGenericDomain createGenericObjectWithLabel:self.completeString];
    }
    
    return ReturnValue;
}

@end
