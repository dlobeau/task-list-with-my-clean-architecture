//
//  KACommandCHangeBooleanValue.h
//  taskList
//
//  Created by Didier Lobeau on 13/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KACommandGeneric.h"

NS_ASSUME_NONNULL_BEGIN

@interface KACommandCHangeBooleanValue : KACommandGeneric

-(id) initWithOwner:(id<KAPresenter>) Owner WithBooleanValue:(BOOL) BooleanValue;

@end

NS_ASSUME_NONNULL_END
