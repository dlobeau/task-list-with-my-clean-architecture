//
//  KAPresenterConformToDomainTest.h
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//



NS_ASSUME_NONNULL_BEGIN

@protocol KADomain;

@protocol KAPresenterConformToDomain 

-(void) expectSender: (id<KAPresenter>) Sender conformToDomain:(id<KADomain>) Domain;

@optional

-(void) expectSender: (id<KAPresenter>) Sender conformToText:(NSString *) ReferenceText;

@end

NS_ASSUME_NONNULL_END
