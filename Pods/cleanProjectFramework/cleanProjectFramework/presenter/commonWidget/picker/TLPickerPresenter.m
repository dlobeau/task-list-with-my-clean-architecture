//
//  TLPickerPresenter.m
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLPickerPresenter.h"
#import "KADate.h"


@implementation TLPickerPresenter

-(NSDate *) date
{
    id<KADate> Date = (id<KADate>) self.data;
    NSAssert([Date conformsToProtocol:@protocol(KADate)], @"picker presenter must be set with date data type, check %@",self.attributeIdentity);
    
    return Date.getDateObject;
}





@end
