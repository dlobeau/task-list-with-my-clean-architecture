//
//  TLPickerPresenter.h
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAGenericPresenter.h"
#import "TLPicker.h"
NS_ASSUME_NONNULL_BEGIN

@interface TLPickerPresenter : KAGenericPresenter<TLPicker>

@end

NS_ASSUME_NONNULL_END
