//
//  TLNewWindowButton.m
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLNewWindowButtonPresenter.h"

@implementation TLNewWindowButtonPresenter

+(NSString *) getTypeTag
{
    return @"newWindowButton";
}

-(id) initWithLabel:(NSString *)label WithLabelIdentifier:(NSString *)ID WithObjectFamilyName:(NSString *)GroupId WithID:(NSString *)TypeId
{
    self = [super initWithLabel:label WithLabelIdentifier:ID WithObjectFamilyName:GroupId WithID:TypeId];
    self.testdelegate = [[TLNewWindowButtonPresenterConformToDomain alloc] init];
    return self;
}

@end


@implementation TLNewWindowButtonPresenterConformToDomain

-(void)expectSender:(id<TLButton>)Sender conformToDomain:(id<KADomain>)Domain
{
    NSAssert(NO, @"not implemented");
}

-(void) expectSender:(id<TLButton>)Sender conformToText:(NSString *)ReferenceText
{
   
}


@end
