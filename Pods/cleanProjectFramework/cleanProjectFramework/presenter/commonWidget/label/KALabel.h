//
//  KALabel.h
//  kakebo
//
//  Created by Didier Lobeau on 20/06/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import "KAPresenter.h"

@protocol KALabel <KAPresenter>

@end
