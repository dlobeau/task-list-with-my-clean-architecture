//
//  BVWidgetLabel.m
//  Skwipy
//
//  Created by Didier Lobeau on 17/12/2015.
//  Copyright (c) 2015 FollowTheDancer. All rights reserved.
//

#import "KALabelPresenter.h"

@interface KALabelPresenter()



@end

@implementation KALabelPresenter

+(NSString *) getTypeTag
{
    return @"Label";
}

-(id) initWithLabel:(NSString *)label WithLabelIdentifier:(NSString *)ID WithObjectFamilyName:(NSString *)GroupId WithID:(NSString *)TypeId
{
    self = [super initWithLabel:label WithLabelIdentifier:ID WithObjectFamilyName:GroupId WithID:TypeId];
     self.testdelegate = [[KALabelPresenterPresenterConformToDomain alloc] init];
     return self;
}


@end

@implementation KALabelPresenterPresenterConformToDomain

-(void)expectSender:(id<KALabel>)Sender conformToDomain:(id<KADomain>)Domain
{
   
}

-(void) expectSender:(id<KALabel>)Sender conformToText:(NSString *)ReferenceText
{
   
}


@end


