//
//  KACheckBoxPresenter.m
//  taskList
//
//  Created by Didier Lobeau on 13/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KACheckBoxPresenter.h"
#import "KABoolean.h"
#import "KASerializeObjectFactory.h"

@interface KACheckBoxPresenter()


@end

@implementation KACheckBoxPresenter

-(id) initWithLabel:(NSString *)label WithLabelIdentifier:(NSString *)ID WithObjectFamilyName:(NSString *)GroupId WithID:(NSString *)TypeId
{
    self = [ super initWithLabel:label WithLabelIdentifier:ID WithObjectFamilyName:GroupId WithID:TypeId];
    self.testdelegate = [[KACheckBoxPresenterTest alloc] init];
    return self;
}

-(BOOL)value
{
    id<KABoolean> Status =(id<KABoolean>) [self data];
    NSAssert([Status conformsToProtocol:@protocol(KABoolean)], @"data must be BOolean type on check box widget, cehck %@", self.attributeIdentity);
   
    return Status.value;
}





@end


@implementation KACheckBoxPresenterTest

-(void) expectSender:(id<KACheckBox>)Sender conformToDomain:(id<KABoolean>)Domain
{
    NSAssert([Domain conformsToProtocol:@protocol(KABoolean)], @"check box test must be done with Boolean, check %@ ",Sender.attributeIdentity);
    NSAssert(Sender.value == Domain.value, @"Error in data binding, check %@",Sender.attributeIdentity);
}

@end
