//
//  KACheckBoxPresenter.h
//  taskList
//
//  Created by Didier Lobeau on 13/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAGenericPresenter.h"
#import "KACheckBox.h"
#import "KAPresenterConformToDomain.h"

NS_ASSUME_NONNULL_BEGIN

@interface KACheckBoxPresenter : KAGenericPresenter<KACheckBox>

@end

@interface KACheckBoxPresenterTest : NSObject< KAPresenterConformToDomain>

@end
NS_ASSUME_NONNULL_END
