//
//  KAEventPageCreationWithDataObservation.m
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAEventPageCreationWithDataObservation.h"
#import "KAPresenter.h"

@implementation KAEventPageCreationWithDataObservation


-(id<KAPresenter>) getDestinationWithOwner:(id<KAPresenter>) Owner
{
    id<KAPresenter> ReturnValue = [super getDestinationWithOwner:Owner];
    
    [Owner subscribeToObservedPresenterDataModificationWithPresenter:ReturnValue];
    
    return ReturnValue;
}

@end
