//
//  KAEvent.h
//  kakebo
//
//  Created by Didier Lobeau on 12/07/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KASeriazableObject.h"

@protocol KAPresenter;
@protocol  KAWindow;
@protocol KADomain;
@protocol KACommand;
@protocol KAView;

@protocol KAEvent <KASeriazableObject>

-(void) doEventActionWithSender:(id<KAPresenter>) Sender;
-(id<KAPresenter>) getDestinationWithOwner:(id<KAPresenter>) Caller;






@end
