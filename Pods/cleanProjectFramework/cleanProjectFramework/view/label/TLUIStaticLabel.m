//
//  TLUIStaticLabel.m
//  taskList
//
//  Created by Didier Lobeau on 30/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUIStaticLabel.h"
#import "KAPresenter.h"

@implementation TLUIStaticLabel

-(BOOL) setContent
{
    if(self.interface != nil)
    {
        NSString *LabelToDisplay  = [self.interface label];
        
        if(LabelToDisplay != nil)
        {
            self.hidden = NO;
            [self setText:LabelToDisplay];
            
        }
        else
        {
            self.hidden = YES;
        }
    }
    return YES;
}

@end
