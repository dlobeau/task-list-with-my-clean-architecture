//
//  KAView.h
//  kakebo
//
//  Created by Didier Lobeau on 31/01/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KAPresenter;
@protocol KADelegatePushNextView;
@protocol KADelegatePushModalView;
@protocol KADataModificationDelegate;

@protocol KAView

//link on the API view object
-(id) view;

//presenter assioaciate to widget
-(id<KAPresenter>) interface;
-(void) setInterface:(id<KAPresenter>) interface;

//called when adding subview to widget
-(BOOL) addChildView:(id<KAView>) ChildView;

-(NSString *) widgetID;

//call to refresh UI widget content
-(BOOL) setContent;

//signal the presenter data should be sent to domain
-(BOOL) validateUserChange;



@optional

//delegate to manage next window display request
-(id<KADelegatePushNextView>) delegatePushNextView;
-(void) setDelegatePushNextView:(id<KADelegatePushNextView>) DelegatePushNextView;




@end
