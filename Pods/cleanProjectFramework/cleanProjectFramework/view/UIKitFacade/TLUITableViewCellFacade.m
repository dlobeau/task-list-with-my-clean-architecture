//
//  TLUITableViewCellFacade.m
//  taskList
//
//  Created by Didier Lobeau on 30/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUITableViewCellFacade.h"
#import "KACell.h"
#import "KAUILabel.h"

@interface TLUITableViewCellFacade()

@property (weak) id<KACell> interface;

@property (weak) id<KADelegatePushNextView> delegatePushNextView;



@end

@implementation TLUITableViewCellFacade

- (void)awakeFromNib
{
    [super awakeFromNib];
    
}

-(UIView *) view
{
    return self.contentView;
}


-(BOOL) setContent
{
    return YES;
}

-(BOOL) validateUserChange
{
    
    return NO;
}

-(BOOL) addChildView:(id<KAView>) ChildView
{
    return YES;
}

- (NSString *)widgetID
{
    return nil;
}

@end
