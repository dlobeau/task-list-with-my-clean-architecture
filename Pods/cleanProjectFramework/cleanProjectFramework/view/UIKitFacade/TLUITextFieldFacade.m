//
//  TLUITextFieldFacade.m
//  taskList
//
//  Created by Didier Lobeau on 30/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUITextFieldFacade.h"
#import "KAPresenter.h"


@interface TLUITextFieldFacade()

@property (weak) id<KAPresenter> interface;
@property (weak) id<KADelegatePushNextView> delegatePushNextView;

@end

@implementation TLUITextFieldFacade

-(UIView *) view
{
    return self;
}

-(BOOL) setContent
{
    if(self.interface != nil)
    {
        NSString *LabelToDisplay  = [self.interface getText];
        [self setText:LabelToDisplay];
    }
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void) textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason
{
    [self.interface addCommandOnLabelModificationWithNewLabel:self.text WithSender:self.interface];
    [self.interface executeCommandHierarchy];
}

-(BOOL) validateUserChange
{
    return NO;
}

-(BOOL) addChildView:(id<KAView>) ChildView
{
    return YES;
}

- (NSString *)widgetID
{
    return self.accessibilityIdentifier;
}

@end
