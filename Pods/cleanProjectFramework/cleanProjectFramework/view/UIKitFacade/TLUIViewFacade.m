//
//  TLUIViewFacade.m
//  taskList
//
//  Created by Didier Lobeau on 13/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUIViewFacade.h"

@interface TLUIViewFacade()

@property id<KADelegatePushNextView> delegatePushNextView;
@property id<KAPresenter> interface;

@end

@implementation TLUIViewFacade

-(id) view
{
    return self;
}

-(BOOL) addChildView:(id<KAView>) ChildView
{
    return NO;
}

-(NSString *) widgetID
{
    return self.accessibilityIdentifier;
}

-(BOOL) setContent
{
    return NO;
}


-(BOOL) validateUserChange
{
    return NO;
}



@end
