//
//  TLTableViewFacade.m
//  taskList
//
//  Created by Didier Lobeau on 30/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUITableViewFacade.h"
#import "KATable.h"
@interface TLUITableViewFacade()



@property (weak) id<KADelegatePushNextView> delegatePushNextView;

@end

@implementation TLUITableViewFacade

-(UIView *) view
{
    return self;
}

-(BOOL) setContent
{
    [self reloadData];
    
    return YES;
}

-(BOOL) validateUserChange
{
    return NO;
}

-(BOOL) addChildView:(id<KAView>) ChildView
{
    return YES;
}

- (NSString *)widgetID
{
    return self.restorationIdentifier;
}


@end
