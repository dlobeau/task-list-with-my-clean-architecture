//
//  TLUIBarButtonFacade.m
//  taskList
//
//  Created by Didier Lobeau on 30/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//



#import "TLUIBarButtonFacade.h"
#import "KAPresenter.h"
#import "KAEvent.h"
#import "KADelegatePushNextView.h"
#import "TLAddBarButton.h"
@interface  TLUIBarButtonFacade()

@property (weak) id<TLAddBarButton> interface;
@property (weak) id<KADelegatePushNextView> delegatePushNextView;

@end
@implementation TLUIBarButtonFacade


-(void) actionFromOwnerWindow:(id<KAView>) OwnerWindow
{
     id<KAPresenter> NextWindow = [self.interface nextAddWindow];
    id<KAView> Window = [NextWindow createViewWithOwner:OwnerWindow];
    
    [self.delegatePushNextView pushNextPage:Window];
}


-(UIView *) view
{
    return nil;
}

-(BOOL ) setContent
{
   self.style = UIButtonTypeContactAdd;
   
    return YES;
}

-(BOOL) validateUserChange
{
    return NO;
}

-(BOOL) addChildView:(id<KAView>) ChildView
{
    return YES;
}

- (NSString *)widgetID
{
    return self.accessibilityIdentifier;
}
@end
