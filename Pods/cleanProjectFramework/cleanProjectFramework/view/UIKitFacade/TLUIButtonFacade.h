//
//  TLUIButtonFacade.h
//  taskList
//
//  Created by Didier Lobeau on 30/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KAView.h"
NS_ASSUME_NONNULL_BEGIN

@interface TLUIButtonFacade : UIButton<KAView>

@end

NS_ASSUME_NONNULL_END
