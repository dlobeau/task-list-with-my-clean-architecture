//
//  KAUINavigationBar.m
//  kakebo
//
//  Created by Didier Lobeau on 17/02/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import "TLUINavigationController.h"
#import "KAPresenter.h"
#import "UIView+KAKEBO.h"

@interface TLUINavigationController ()

    @property id<KAView> mainRootWindow;

@end


@implementation TLUINavigationController

@synthesize mainRootWindow = _mainRootWindow;


#pragma KAView protocole

-(BOOL ) setContent
{
    NSAssert(self.interface != nil, @"Bar button presenter object can't be null");
    
    [self setTitle:[self.interface label]];
    
    [self.mainRootWindow setContent];
    
    return YES;
}


-(BOOL) addChildView:(id<KAView>)ChildView
{
    BOOL returnValue = [self.mainRootWindow addChildView:ChildView] ;
    return returnValue;
}

-(NSString *) widgetID
{
    return self.mainRootWindow.widgetID;
}

-(id<KAView>) mainRootWindow
{
    if(_mainRootWindow == nil)
    {
        NSArray<id<KAView>> *List = [self viewControllers];
        _mainRootWindow = [List objectAtIndex:0];
        self.tabBarItem = [(UIViewController *)_mainRootWindow tabBarItem];
    }
    return _mainRootWindow;
}

-(void) setMainRootWindow:(id<KAView>)mainRootWindow
{
    _mainRootWindow = mainRootWindow;
}

-(BOOL) validateUserChange
{
    return [self.mainRootWindow validateUserChange];
}


#pragma KAPushNextPAgeDElegate
-(void) pushNextPage:(UIViewController *) nextPage
{
    [self pushViewController:nextPage animated:YES];
}

-(id<KAPresenter> )interface
{
    return self.mainRootWindow.interface;
}

-(void) setInterface:(id<KAPresenter>)interface
{
    [ self.mainRootWindow setInterface:interface];
}



-(id<KADelegatePushNextView>) delegatePushNextView
{
    return self;
}

-(void) setDelegatePushNextView:(id<KADelegatePushNextView>)DelegatePushNextView
{
    
}


@end
