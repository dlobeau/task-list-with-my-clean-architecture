//
//  TLUILabeFacade.m
//  taskList
//
//  Created by Didier Lobeau on 30/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUILabelFacade.h"
#import "KALabel.h"
@interface TLUILabelFacade()

@property (weak) id<KALabel> interface;
@property (weak) id<KADelegatePushNextView> delegatePushNextView;
@end

@implementation TLUILabelFacade

-(UIView *) view
{
    return self;
}

-(BOOL) setContent
{
    if(self.interface != nil)
    {
        NSString *LabelToDisplay  = [self.interface getText];
        
        if(LabelToDisplay != nil)
        {
            self.hidden = NO;
            [self setText:LabelToDisplay];
            
        }
        else
        {
            self.hidden = YES;
        }
    }
    return YES;
}

-(BOOL) validateUserChange
{
    return NO;
}

-(BOOL) addChildView:(id<KAView>) ChildView
{
    return YES;
}

- (NSString *)widgetID
{
    return self.accessibilityIdentifier;
}

@end
