//
//  TLUISaveTaskButton.h
//  taskList
//
//  Created by Didier Lobeau on 30/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUIButtonFacade.h"

NS_ASSUME_NONNULL_BEGIN

@interface TLUISaveTaskButton : TLUIButtonFacade

@end

NS_ASSUME_NONNULL_END
