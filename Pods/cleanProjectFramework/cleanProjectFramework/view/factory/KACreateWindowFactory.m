//
//  KACreateWindowFactory.m
//  kakebo
//
//  Created by Didier Lobeau on 15/11/2018.
//  Copyright © 2018 imhuman. All rights reserved.
//

#import "KACreateWindowFactory.h"
#import "TLUIFacadeViewController.h"
#import "TLUINavigationController.h"
#import "UIView+KAKEBO.h"

@implementation KACreateWindowFactory

-(id<KAView> ) createWithOwner:(id<KAView>) Owner WithWidgetType:(NSString * ) WidgetType WithWidgetIdentifier:(NSString *) identifier  WithSender:(id<KAPresenter>) Sender
{
    TLUIFacadeViewController* ViewController = (TLUIFacadeViewController*)[super createWithOwner:Owner WithWidgetType:WidgetType WithWidgetIdentifier:identifier WithSender:Sender];
    
    TLUINavigationController* ReturnValue = [[TLUINavigationController alloc]
                             initWithRootViewController:(UIViewController *)ViewController];
    
  //  [(UIView *)ReturnValue.navigationBar setBackgroundColor:[UIColor clearColor]];
    
    ViewController.delegatePushNextView = ReturnValue;
    
    return ReturnValue;
    
}

-(id<KAView>) getChildWithID:(NSString *) ID WithWidgetParent:(TLUINavigationController *) WidgetParent
{
    id<KAView> returnValue = nil;
    
   UIView * ParentView = WidgetParent.view;
    
    if ([WidgetParent respondsToSelector:@selector(mainRootWindow)]) {
        ParentView = WidgetParent.mainRootWindow.view;
    }
    
    
    returnValue = [ParentView getChildWithID:ID];
    
    return returnValue;
    
}

@end
