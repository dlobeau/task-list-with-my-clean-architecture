//
//  KALoginInformation.h
//  taskList
//
//  Created by Didier Lobeau on 24/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol KACompleteItemIdentification <NSObject>

-(NSString *) attributeIdentity;

-(NSString *) label;
-(void) setLabel:(NSString *) Label;

-(NSString *) labelIdentifier;
-(void) setLabelIdentifier:(NSString *) LabelIdentifier;

-(NSString *) objectFamilyName;

-(NSString *) ID;
-(void) setID:(NSString *) ID;

@end

NS_ASSUME_NONNULL_END
