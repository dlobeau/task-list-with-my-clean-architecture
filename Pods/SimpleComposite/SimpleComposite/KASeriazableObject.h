//
//  KAAttribute.h
//  kakebo
//
//  Created by Didier Lobeau on 09/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KAComparable.h"
#import "KATreeNavigable.h"
#import "KACompleteItemIdentification.h"
#import "KASerializable.h"

@protocol KASeriazableObjectTable;
@protocol KAFile;
@protocol KASerializeObjectFactory;

@protocol KASeriazableObject<KAComparable,KATreeNavigable,KACompleteItemIdentification,KASerializable>

-(BOOL) isDeleted;
-(void) setIsDeleted:(BOOL) isDeleted;


-(BOOL) emptyChildList;
-(BOOL) removeChild:(id<KASeriazableObject> ) ChildObject;
-(BOOL) removeAllChild:(NSArray<id<KASeriazableObject>>* ) ChildObjectList;

-(void) addDataForDynamicChildsIteration:(id<KASeriazableObject>) Table;
-(NSMutableArray<id<KASeriazableObjectTable>>*) getDataForDynamicChildIterationWithID:(NSString *) ID;

-(BOOL) addCloneOfChild:(id<KASeriazableObject> ) ChildObject WithTypeId:(NSString *) Type_id;
-(BOOL) addLinkToChild:(id<KASeriazableObject> ) ChildObject WithTypeId:(NSString *) Type_id;

-(id<KASeriazableObject> ) getChildwithTypeId:(NSString *) TypeId WithDynamicallyCreatedChildsInclude:(BOOL) isDynamicChildsInclude;
-(id<KASeriazableObject> ) getChildwithIdentifier:(NSString *) Identifier;

-(NSArray<id<KASeriazableObject>> *) childs;


-(NSString *) toFile:(id<KAFile>) File;
-(NSDictionary *) toDictionnary;

-(id<KASeriazableObject>) cloneObject;
-(id<KASeriazableObject>) cloneObjectWithoutChild;

- (NSString *) refreshedPath;

-(BOOL) isParentOfChild:(id<KASeriazableObject>) Child;

-(BOOL) isSerializable;
-(void) setIsSerializable:(BOOL) isSerializable;


-(id<KASerializeObjectFactory>) factoryDelegate;
-(void) setFactoryDelegate:(id<KASerializeObjectFactory>) factoryDelegate;

-(id<KASeriazableObject> ) nodeFromPathString:(NSString *) PathString;
-(NSArray<id<KASeriazableObject>> *) nodeListFromPathString:(NSString *) PathString;

-(BOOL) addChild:(id<KASeriazableObject> ) ChildObject;
-(BOOL) addChild:(id<KASeriazableObject> ) ChildObject WithTypeId:(NSString *) TypeId;
-(BOOL) addAllChild:(NSArray<id<KASeriazableObject>> *) ChildListObject;

-(NSArray<id<KASeriazableObject>> *) getChildListWithGroupId:(NSString *) GroupId;

-(id<KASeriazableObject> ) getChildwithTypeId:(NSString *) TypeId;


///KATreeNavigable protocol //////////////
-(id<KASeriazableObject>) parent;
-(void) setParent:(id<KASeriazableObject>) Parent;
@end
