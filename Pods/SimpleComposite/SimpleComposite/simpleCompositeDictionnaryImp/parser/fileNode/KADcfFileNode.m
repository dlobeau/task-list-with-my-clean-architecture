//
//  KADcfFileNode.m
//  kakebo
//
//  Created by Didier Lobeau on 01/08/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KADcfFileNode.h"
#import "KADcfNodeFactory.h"
#import "KABasicFile.h"
#import "KASeriazableObjectImp.h"
#import "KASerializeObjectFactory.h"
@implementation KADcfFileNode

-(id) initWithDictionary:(NSDictionary *) Dictionary WithNodeName:(NSString *)NodeName
{
    KADcfFileNode *ReturnValue = [super initWithDictionary:Dictionary WithNodeName:NodeName];
    
    return ReturnValue;
}

-(NSString *) getFileNameFromNode
{
    NSString *ReturnValue = nil;
    
    if(self.dictionary != nil)
    {
        NSString * Filename = [self.dictionary objectForKey:[KASeriazableObjectImp getLabelTag]];
        
        KABasicFile *F = [self.factoryDelegate createFileWithFileName:Filename];
        
        ReturnValue = [F getFileCompleteName];
    }
    return ReturnValue;
}


-(id<KASeriazableObject>) ParseNodeWithPath:(NSString *) Path
{
    id<KASeriazableObject>  ReturnValue = nil;
     KADcfGenericNode * N = nil;
    NSString *FileName = [self getFileNameFromNode];
    NSAssert(FileName != nil, @"Filename : %@ defined in node %@@",[self.dictionary objectForKey:[KASeriazableObjectImp getLabelTag]],Path );
    
    NSArray *rawRootArray = [[NSArray alloc] initWithContentsOfFile:FileName];
    
    if( (rawRootArray != nil) && ([rawRootArray count] == 1))
    {
        NSDictionary *CurrentDictionary = [rawRootArray objectAtIndex:0];
        
        KADcfNodeFactory *FactoryNode = [[KADcfNodeFactory alloc] init];
        
        N = [FactoryNode createParserNodeFromObject:CurrentDictionary WithNodeName:@"item 0"];
        N.factoryDelegate = self.factoryDelegate;
        ReturnValue = [super ParseNode:N WithPath:Path];
        
        if(ReturnValue != nil)
        {
            [ReturnValue setID:[self.dictionary objectForKey:[KASeriazableObjectImp getTypeIdTag]]];
        }
    
    }

    return ReturnValue;
}


@end
