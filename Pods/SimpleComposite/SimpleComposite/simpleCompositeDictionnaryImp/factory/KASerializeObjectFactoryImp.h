//
//  KAAGenericAttributeFactory.h
//  kakebo
//
//  Created by Didier Lobeau on 13/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KASerializeObjectFactory.h"

@class KASeriazableObjectImp;

@interface KASerializeObjectFactoryImp : NSObject <KASerializeObjectFactory>


-(id) initWithInjectionDependencyFileNameList:(NSArray<NSString *> *)injectionDependencyFileNameList WithBundle:(NSBundle *) bundle;

@end
