//
//  KAFile.h
//  genericSerializedObject
//
//  Created by Didier Lobeau on 23/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol KAFile <NSObject>

-(NSString *) completeFilePathAndName;
-(NSString *) fileName;

-(BOOL) isFileExist;

-(NSString *) getFileCompleteName;

-(void) cleanFileFromUserEnvironment;
+(NSString *) environmentRoot;
-(NSString * ) copyRessourceFileWithName:(NSString *) FileNameWithoutExtension;
-(NSString * ) copyRessourceFileWithName:(NSString *) FileNameWithoutExtension WithFolderDestination:(NSString *) FolderName;
-(NSString *) copyFileWithName:(NSString *) FileNameWithoutExtension ToDestination:(NSString *) Destination;

@end

NS_ASSUME_NONNULL_END
