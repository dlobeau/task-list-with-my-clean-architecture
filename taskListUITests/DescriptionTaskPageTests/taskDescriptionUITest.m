//
//  taskDescriptionUITest.m
//  taskListUITests
//
//  Created by Didier Lobeau on 30/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TestEnvironmentSettings.h"
#import "TLTasksDataBase.h"
#import "TLTask.h"
#import "TLTasksListApplication.h"

@interface taskDescriptionUITest : XCTestCase

@end

@implementation taskDescriptionUITest

- (void)setUp
{
    
    TestEnvironmentSettings * TestEnvironment = [[TestEnvironmentSettings alloc] init];
    
    id<TLTasksListApplication> app = (id<TLTasksListApplication>) [TestEnvironment application];
    id<TLTasksDataBase>dataBase = [app taskDataBase];
    id<TLTask> newTask = [dataBase createNewTask];
    
    
    NSMutableDictionary *Env = [[NSMutableDictionary alloc] init];
    [Env setObject:@"TaskDescriptionUITest" forKey:@"page"];
    [Env setObject:@"UITEST" forKey:@"mode"];
    [Env setObject:newTask.toString forKey:@"dataBinding"];
    
    XCUIApplication *AAP =  [[XCUIApplication alloc] init];
    [AAP setLaunchEnvironment:Env];
    
    self.continueAfterFailure = NO;
    
    [AAP  launch];

}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testTaskDescriptionUI_windowIntialization
{
    
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    
   
    
}

@end
