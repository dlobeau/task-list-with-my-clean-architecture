//
//  taskListUITests.m
//  taskListUITests
//
//  Created by Didier Lobeau on 22/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <XCTest/XCTest.h>


@interface taskListUITests : XCTestCase

@end

@implementation taskListUITests

- (void)setUp
{
    NSMutableDictionary *Env = [[NSMutableDictionary alloc] init];
    [Env setObject:@"TaskListUITests" forKey:@"page"];
    [Env setObject:@"UITEST" forKey:@"mode"];
    
    XCUIApplication *AAP =  [[XCUIApplication alloc] init];
    [AAP setLaunchEnvironment:Env];
    
    self.continueAfterFailure = NO;
    
    [AAP  launch];
}

- (void)tearDown
{
    
}

- (void)testtaskListUI_windowIntialization
{
    
     
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [app.navigationBars[@"TLUITasksListView"].buttons[@"Add"] tap];
    [app.textFields[@"editBoxWithName"] tap];
    
    XCUIElement *tKey = app/*@START_MENU_TOKEN@*/.keys[@"t"]/*[[".keyboards.keys[@\"t\"]",".keys[@\"t\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/;
    [tKey tap];
    [tKey tap];
    
    XCUIElement *oKey = app/*@START_MENU_TOKEN@*/.keys[@"o"]/*[[".keyboards.keys[@\"o\"]",".keys[@\"o\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/;
    [oKey tap];
    [oKey tap];
    [tKey tap];
    [tKey tap];
    [oKey tap];
    [oKey tap];
    [app/*@START_MENU_TOKEN@*/.buttons[@"Return"]/*[[".keyboards",".buttons[@\"return\"]",".buttons[@\"Return\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/ tap];
    [app/*@START_MENU_TOKEN@*/.buttons[@"deadlineButton"]/*[[".buttons[@\"12 November 2019\"]",".buttons[@\"deadlineButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ tap];
    
     [app.datePickers.pickerWheels[@"November"] swipeUp];
    
    XCUIElement *button = app/*@START_MENU_TOKEN@*/.buttons[@"button"]/*[[".buttons[@\"Save\"]",".buttons[@\"button\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/;
    [button tap];
    [button tap];
    
   
    
    
    
   
  
    
}

@end
